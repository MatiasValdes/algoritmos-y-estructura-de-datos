#include <iostream>
#include <unistd.h>
#include <fstream>
#include <vector>
#include "ArbolAvl.h"
#include "Grafo.h"

// Funcion que nos leerá el archivo y nos entregará un vector con los datos
vector<string> leer_archivo(int opcion){
    // Variables de apoyo dentro de la función
    fstream archivo;
    string tmp;
    vector<string> ids_prote;
    // Si el usuario decidió agregar todas las proteinas se abre el archivo correspondiente
    if(opcion == 1){
        // Abrimos el archivo
        archivo.open("pdbs_unidos.txt", ios::in);
    }
    // Sino abrimos el archivo con menos id's
    else{
        archivo.open("fraccion.txt", ios::in);
    }
    // Si no se puede abrir el archivo se entrega el mensaje de error
    if(archivo.fail()){
		cout<<"No se pudo abrir el archivo";
		exit(1);
	}
    // Sino se empieza a agregar las id's de las proteínas al vector
    while(getline(archivo, tmp)){
        ids_prote.push_back(tmp);
    }
    // Cerramos archivo y retornamos el vector
    archivo.close();
    return ids_prote;
}


// Funcion del menú
int menu(){
    string opcion;
    // Se imprimen las opciones del usuario
    cout << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "Ingrese una de las siguientes opciones" << endl;
    cout << "[1] Insertar id al arbol" << endl;
    cout << "[2] Eliminar id del arbol" << endl;
    cout << "[3] Modificar un id del arbol" << endl;
    cout << "[4] Mostrar contenido del arbol " << endl;
    cout << "[5] Agregar ids del archivo pdb completo" << endl;
    cout << "[6] Agregar ids del archivo pdb pero una parte fraccionada" << endl;
    cout << "[7] Generar el grafo" << endl;
    cout << "[8] Salir del programa" << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "¿Que desea hacer?  ";
    // Capturamos la opcion y la devolvemos en formato numerico
    getline(cin, opcion);
    return stoi(opcion);

}

// Funcion que nos convertira los strings a mayusculas
string toMayusculas(string mayus){
    // Ciclo para recorrer el string
    for(int i=0;i<mayus.length();i++){
        // Cambiamos las letras del string a mayusculas
        mayus[i] = toupper(mayus[i]);
    }
    // Retornamos la palabra
    return mayus;
}


// Funcion principal
int main(int argc, char* argv[]){
    // Variables de apoyo para el codigo
    int opcion=1, verificador=1;
    string line;
    string elemento;
    string id = "";
    string eliminar = "\0";
    string editar = "\0";
    bool altura = false;
    // Creamos un nodo para utilizarlo como la raiz de nuestro arbol
    Nodo *raiz = NULL;
    // Y creamos nuestro arbol binario
    ArbolAvl *arbol_balanc = new ArbolAvl();
    // Creamos un vector para guardar las id's
    vector<string> ids_prote;
    // Se crea el grafo
    Grafo *grafo;

    while(opcion != 8){
        // Capturamos la opcion del menú
        opcion = menu();
        system("clear");
        // Opcion 1 para agregar un elemento
        if(opcion == 1){
            cout << "Para dejar de agregar ids, escriba stop" << endl;
            while(verificador != 0){
                cout << "Ingrese el ID de la proteina" << endl;
                getline(cin, id);
                // LLamamos a la función que nos devolverá el string en mayusculas
                id = toMayusculas(id);
                if(id != "STOP"){
                    // Mientras no sea el stop, se ingresara el nodo
                    altura = false;
                    arbol_balanc->insertar_nodo(raiz, id, altura);
                    // Volvemos a "0" la variable de la id
                    id = "\0";
                }
                else{
                    // Dejamos de agregar datos
                    verificador = 0;
                }
            }
        }
        // Opcion para eliminar un elemento
        else if(opcion == 2){
            // Nos aseguramos que el arbol no se encuentre vacio
            if(raiz != NULL){
                // Mostramos los elementos que contiene el arbol
                cout << "A continuación se mostrarán los nodos del arbol para elegir el que se desea eliminar" << endl;
                arbol_balanc->inorden(raiz);
                cout << endl;
                // Pedimos el dato a eliminar
                cout << "Ingrese el id a eliminar: "; 
                getline(cin, line);
                // Cambiamos a mayusculas el id y lo entregamos a eliminar
                eliminar = toMayusculas(line);
                // Se llama la funcion de eliminar el nodo
                arbol_balanc->eliminar_nodo(raiz, eliminar, altura);
                eliminar = "\0";
            }
            else{
                cout << "El arbol se encuentra vacío" << endl << endl;
            }
        }
        // Modificar un elemento del arbol
        else if(opcion == 3){
            // Nos aseguramos que el arbol no se encuentre vacio
            if(raiz != NULL){
                // Mostramos el arbol como se encuentra actualmente
                cout << "A continuación se mostrarán los nodos del arbol para elegir el que se desea eliminar" << endl;
                arbol_balanc->inorden(raiz);
                cout << endl;
                // Pedimos el id a editar y el id que se actualizara en esa posicion
                cout << "Ingrese el id a editar: "; 
                getline(cin, line);
                // Transformamos el line a mayusculas y se lo damos a eliminar
                eliminar = toMayusculas(line);
                // Pedimos el id a eliminar
                cout << "Ingrese el id que querrá actualizar en esa posicion: "; 
                getline(cin, line);
                // Transformamos el line a mayusculas y se lo damos a editar
                editar = toMayusculas(line);
                // Si el id se encuentra
                if(arbol_balanc->buscar_nodo(raiz, eliminar) == true){
                    // Se llama la funcion de eliminar el nodo
                    arbol_balanc->eliminar_nodo(raiz, eliminar, altura);
                    // Se crea el nuevo nodo
                    arbol_balanc->insertar_nodo(raiz, editar, altura);
                    cout << "Se ha generado el cambio que ud necesitaba" << endl;
                }
            }
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
        }
        else if(opcion == 4){
            // Nos aseguraremos de que no este vacio
            if(raiz != NULL){
                cout << endl << endl;
                // Se mostraran todas las posibles de recorrido del arbol
                cout << "-------- ARBOL ACTUAL --------" << endl << endl;
                cout << "----- METODO INORDEN -----" << endl;
                arbol_balanc->inorden(raiz);
                cout << endl << endl;
            }
            else{
                cout << "El arbol se encuentra vacío " << endl;
            }
        }
        // Opcion que nos leerá las id
        else if(opcion == 5){
            // Variable que nos ayudará a abrir todo el archivo
            int var=1;
            // Guardamos el vector que nos entrega la función
            ids_prote = leer_archivo(var);
            // Ciclo para ir agregando las id's
            for(int i=0; i<ids_prote.size();i++){
                altura = false;
                elemento = ids_prote[i];
                // Insertamos el nodo
                arbol_balanc->insertar_nodo(raiz,elemento, altura);
            }
            
        }
        // Opcion que nos leerá las id's de archivo fraccionado
        else if(opcion == 6){
            // Variable que nos indicará que se habrá el archivo más pequeño
            int var=0;
            // Guardamos el vector que nos entrega la función
            ids_prote = leer_archivo(var);
            // Ciclo para ir agregando las id's
            for(int i=0; i<ids_prote.size();i++){
                altura = false;
                elemento = ids_prote[i];
                // Insertamos el nodo
                arbol_balanc->insertar_nodo(raiz,elemento, altura);
            }
        }
        // Crear el grafico del arbol
        else if(opcion == 7){
            // Si el arbol no se encuentra vacio crearemos un grafico
            if(raiz != NULL){
                // Se crea el nuevo grafico enviando la raiz de nuestro arbol
                grafo = new Grafo(raiz);
                // Volvemos nuestro verificador a cualquier numero por si desea seguir agregando datos
                verificador = 10;
            }
            // Sino solo se pasa
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
        }
        // Opcion para salir del programa
        else if(opcion == 8){
            cout << "Gracias por utilizar el programa" << endl;
            return 0;
        }
        // Si se entrega una opcion no valida numerica
        else{
            cout << "Ingrese una opcion valida" << endl;
        }

    }
    return 0;
}