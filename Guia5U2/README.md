# Guia Numero 5 unidad II

## Descripcion del programa 
El programa cuenta con la creación de un arbol binario balanceado, en el cual el usuario puede agregar id's de proteinas, eliminar id's de proteinas, modificar id's generando una eliminación y luego la inserción, cuenta con dos opciones en la cual el usuario puede decidir si agregar aproximadamente 170000 id's de proteínas o solamente una fracción de ellos (~60) y además cuenta con la opción de generar un grafo con su arbol en formato png

## Problemas en el programa
El programa presenta un pequeño problema cuando el usuario desea ver el contenido del arbol en metodo inorden luego de agregar las id's desde el archivo y aunque no era necesario en la guía lo deje puesto ya que encontré necesario cuando el usuario quiera ingresar el mismo sus id's para que tenga un orden de lo que está ingresando.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
* Paquete graphviz

En caso de tener los requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

### ¿Como instalar graphviz?
Para instalar graphviz, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install graphviz
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el paquete. Una vez termine la descarga, el paquete estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./programa
```

## Funcionamiento del programa
El programa iniciará mostrando un menú el cual es el siguiente
* [1] Insertar id al arbol
* [2] Eliminar id del arbol
* [3] Modificar un id del arbol
* [4] Mostrar contenido del arbol
* [5] Agregar ids del archivo pdb completo
* [6] Agregar ids del archivo pdb pero una parte fraccionada
* [7] Generar el grafo
* [8] Salir del programa


Dependiendo de la opción que elija el programa hará las cosas.


### Opción 1
El programa empezará a agregar id's hasta que se le indique  escribiendo "stop" que se dejen de agregar id's, en este inciso el programa no dejará agregar id's repetidos ya que deben ser unicos

### Opcion 2
El programa mostrará el arbol actual y el usuario solo debe ingresar el id que desea eliminar

### Opcion 3
El programa le solicitará el id para editar y luego pedirá el id que quiere que sea agregado. En este punto el programa eliminará y luego agregara el dato

### Opcion 4
Cuando el usuario ingrese la opcion se mostrara el arbol en metodo inorden en la terminal

### Opcion 5
El programa agregará los id's de las proteínas que son alrededor de 170000 id's desde el archivo, el desarrollador no pudo generar la imagen con tantos id's ya que era muy pesada la imagen

### Opcion 6
El programa agregará una fracción del archivo completo de las id's de las proteínas que son un alrededor de 60 id's para que el usuario pueda ver una visualización clara de como se genera un arbol balanceado con las id's de las proteínas

### Opcion 7
El programa generara una imagen con el arbol actual que el usuario haya creado, esta imagen será en formato png

### Opcion 8
El programa simplemente finalizará por lo que se recomienda generar una imagen antes de realizar esta opción.

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl