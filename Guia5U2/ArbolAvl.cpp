#include <iostream>
#include <unistd.h>
#include <fstream>
#include "ArbolAvl.h"

using namespace std;

// Constructor por defecto de la clase arbol
ArbolAvl::ArbolAvl(){
}

// Metodo para crear nodo
Nodo* ArbolAvl::crear_nodo(string dato){
	Nodo *tmp;
	// Se crea nodo
    tmp = new Nodo;
	// Se asigna la info al nodo que se desea guardar
	tmp->id = dato;
    // Por defecto, la izquierda y la derecha apuntarán a nulo
	tmp->izq = NULL;
	tmp->der = NULL;
	// Y el factor de equilibrio en 0
	tmp->FactorE = 0;
	return tmp;
}

// Metodo para buscar el nodo lo que nos entregará un true o un false
bool ArbolAvl::buscar_nodo(Nodo *tmp, string eliminar){
	// Se empezará a buscar el nodo
	// Si el nodo es menor por la izquierda, se busca por izq
	if(eliminar < tmp->id){
		// Si el nodo está apuntando a nulo es porque no existe el nodo
		if(tmp->izq == NULL){
			cout << "No está el id" << endl;
			return false;
		}
		else{
			// Si no es nulo es porque quizás existe el nodo por lo tanto
			// se debe seguir buscando por izquierda
			buscar_nodo(tmp->izq, eliminar);
		}
	}
	// Si el id no es menor por la izquierda, será mayor por derecha
	else{
		// Nos aseguraremos de que el id sea mayor y se busque
		if(eliminar > tmp->id){
			// Seguimos el mismo procedimiento que por izquierda
			if(tmp->der == NULL){
				cout << "No está el id" << endl;
				return false;
			}
			else{
				buscar_nodo(tmp->der, eliminar);
			}
		} // Pero si no es ni mayor por derecha ni por izquierda nos da a pensar que es el mismo id
		else{
			// Por lo tanto, damos un mensaje de encontrarlo y retornamos un true
			cout << "Se encontró el id a eliminar" << endl;
			return true;
		}
	}
}


// Metodo para insertar nodo
void ArbolAvl::insertar_nodo(Nodo *&tmp, string dato, bool &altura){
	// Primero nos aseguramos de que el temporal no este nulo
	if(tmp != NULL){
		// Si el valor es menor que el valor actual, indica que ingresa a la izquierda
		if(dato.compare(tmp->id) < 0){
			// Llamado a la funcion de insertar nodos
			insertar_nodo(tmp->izq, dato, altura);
			// Si la altura es verdadera
			if(altura == true){
				switch(tmp->FactorE){
					// FE == 1
					case 1:
						// La derecha tiene un nivel mas grande que el de la izquierda, por lo tanto 
						// si agregamos a la izquierda tendremos que ambos tendrán el mismo level
						tmp->FactorE = 0;
						// Altura es falsa
						altura = false;
						break;
					// FE == 0
					case 0:
						// Tenemos el mismo nivel en ambos lados y se añade uno a la izquierda
						tmp->FactorE = -1;
						break;
					// FE == -1
					case -1:
						// La izquierda tiene un nivel mas grande que la derecha por lo tanto se desbalanceará
						// el arbol si se añade uno mas a la izquierda
						// El nodo tomará el nodo de la izquierda para empezar la reestructuración
						nodo1 = tmp->izq;
						// Rotacion
						// Vemos si el factorE es menor a 0
						if(nodo1->FactorE <= 0){
							// El nodo de la izquierda recibe a su hijo y apunta al nuevo y antecesor
							tmp->izq = nodo1->der;
							nodo1->der = tmp;
							// Cambiamos el factorE
							tmp->FactorE = 0;
							tmp = nodo1;
						}
						else{
							// Rotacion ID
							// El nodo pasa a ser el nodo raiz
							nodo2 = nodo1->der;
							tmp->izq = nodo2->der;
							nodo2->der = tmp;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
							// Cuando realizamos la rotación debemos actualizar el factor de equilibrio
							if(nodo2->FactorE == -1){
								tmp->FactorE = 1;
							}
							else{
								tmp->FactorE = 0;
							}
							// Debemos seguir realizando la reestructuracion tomando el nodo raiz como el ultimo rotado
							tmp = nodo2;
						}
						// Actualizamos el factorE de la raíz
						tmp->FactorE = 0;
						altura = false;
						break;
					}
				}
			}
			else{
				// Si el dato es mayor que el valor del raiz, se asume que indica el ingreso a la derecha
				if(dato.compare(tmp->id) > 0){
					// Inserción por la derecha
					insertar_nodo(tmp->der, dato, altura);
					if(altura == true){
						switch(tmp->FactorE){
							// Izquierda es de un nivel superior que el de la derecha, cuando añadamos uno a la derecha
							// el arbol se equilibrará
							case -1:
								tmp->FactorE = 0;
								altura = false;
								break;
							// Ambos lados del arbol están balanceados
							case 0:
								tmp->FactorE = 1;
								break;
							// El lado derecho del arbol es mas grande que la izquierda por lo que si agregamos
							// agregamos a la derecha se desbalanceará
							case 1:
								// Reestructuración
								nodo1 = tmp->der;
								if(nodo1->FactorE >= 0){
									// Rotacion DD
									// Nodo que recibe a su nodo hijo que apunta al menor y a la derecha a su nuevo nodo
									tmp->der = nodo1->izq;
									nodo1->izq = tmp;
									tmp->FactorE = 0;
									tmp = nodo1;
								}
								else{
									// Rotacion DI
									// El nodo pasa a ser el nodo de la raiz para apuntar a la rama mas pequeña y por
									// la derecha al que se encontraba antes
									nodo2 = nodo1->izq;
									tmp->der = nodo2->izq;
									nodo2->izq = tmp;
									nodo1->izq = nodo2->der;
									nodo2->der = nodo1;
									// Debemos actualizr los factores de equilibrio
									if(nodo2->FactorE == 1){
										tmp->FactorE = -1;
									}
									else{
										tmp->FactorE = 0;
									}
									if(nodo2->FactorE == -1){
										nodo1->FactorE = 1;
									}
									else{
										nodo1->FactorE = 0;
									}
									// Debemos seguir realizando la reestructuracion tomando el nodo raiz como el ultimo rotado
									tmp = nodo2;
								}
								// Volvemos el factorE a 0 
								tmp->FactorE = 0;
								altura = false;
								break;
						}
					}
				}
				else{
					cout << " Id agregado previamente " << endl;
				}
			}
		}
		// Creacion del nodo
		else{
			// Creamos el nodo
			tmp = crear_nodo(dato);
			altura = true;
			cout << "El id fue agregado con exito"<< endl;
		}
}


// Funcion para reestructurar la izquierda
void ArbolAvl::reestructuraIzq(Nodo *&tmp, bool &altura){
	if(altura == true){
		switch(tmp->FactorE){
			// Si eliminamos por la izquierda, las ramas quedarán balanceadas
			case -1:
				tmp->FactorE = 0;
				break;
			// A eliminar por la izquierda, la rama de la derecha quedará con un nivel superior	
			case 0:
				tmp->FactorE = 1;
				altura = false;
				break;
			// Reestructuración del arbol ya que la derecha tenía nivel superior y al eliminar a la izquierda el arbol se desbalancea
			case 1:
				nodo1 = tmp->der;
				// Rotación DD
				if(nodo1->FactorE >= 0){
					tmp->der = nodo1->izq;
					nodo1->izq = tmp;
					if(nodo1->FactorE == 0){
						tmp->FactorE = 1;
						nodo1->FactorE = -1;
                        altura = false;
                    }
                    if(nodo1->FactorE == 1){
                        tmp->FactorE = 0;
                        nodo1->FactorE = 0;
                    }
                    tmp = nodo1;
				}
				// Rotación DI
				else{
					nodo2 = nodo1->izq;
					tmp->der = nodo2->izq;
					nodo2->izq = tmp;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
					// Actualización de factores de equilibrio
					if(nodo2->FactorE == 1){
						tmp->FactorE = -1;
					}
					else{
						tmp->FactorE = 0;
					}
					if(nodo2->FactorE == -1){
						nodo1->FactorE = 1;
					}
					else{
						nodo1->FactorE = 0;
					}
					// Actualización del nodo analizado
					tmp = nodo2;
					nodo2->FactorE = 0;
				}
				// Volvemos a tener los factores anteriores
				tmp->FactorE = 0;
				altura = false;
				break;
			}
		}
}

// Metodo para reestructurar la derecha
void ArbolAvl::reestructuraDer(Nodo *&tmp, bool &altura){
	if(altura == true){
		switch(tmp->FactorE){
			// La rama derecha tenía un nivel superior y al eliminar, las dos quedaron igual
			case 1:
				tmp->FactorE = 0;
				break;
			// Las dos ramas tenían mismo nivel pero al eliminar derecha, la izquierda es mayor
			case 0:
				tmp->FactorE = -1;
				altura = false;
				break;
			// La izquierda tenía nivel superior y al eliminar a la derecha hay q reestructurar el arbol
			case -1:
			// Reestructuración del arbol
				nodo1 = tmp->izq;
				// Rotacion II
				if(nodo1->FactorE <= 0){
					tmp->izq = nodo1->der;
					nodo1->der = tmp;
					if(nodo1->FactorE == 0){
						tmp->FactorE = -1;
						nodo1->FactorE = 1;
						altura = false;
					}
					if(nodo1->FactorE == -1){
						tmp->FactorE = 0;
						nodo1->FactorE = 0;
					}
					tmp = nodo1;
				}
				// Rotacion ID
				else{
					nodo2 = nodo1->der;
					tmp->izq = nodo2->der;
					nodo2->der = tmp;
					nodo1->der = nodo2->izq;
					nodo2->izq = aux;
					// Actualizacion de factores de equilibrio
					if(nodo2->FactorE == -1){
						tmp->FactorE = 1;
					}
					else{
						tmp->FactorE = 0;
					}
					if(nodo2->FactorE == 1){
						nodo1->FactorE = -1;
					}
					else{
						nodo1->FactorE = 0;
					}
					// Actualizamos nuestro nodo reestructurado
					tmp = nodo2;
					nodo2->FactorE = 0;
				}
		}
	}
}


// Metodo para eliminar un nodo
void ArbolAvl::eliminar_nodo(Nodo *&tmp, string dato, bool &altura){
	// Que no este vacio
	if(tmp != NULL){
		// Si el dato es menor a la id
		if(dato.compare(tmp->id) < 0){
			// Hacemos recursividad por izquierda
			eliminar_nodo(tmp->izq, dato, altura);
			// Y reestructuramos 
			reestructuraIzq(tmp, altura);
		}
		else{
			// Si el dato es mayor a la id
			if(dato.compare(tmp->id) > 0){
				// Hacemos recursividad por derecha
				eliminar_nodo(tmp->der, dato, altura);
				// Y reestructuramos
				reestructuraDer(tmp, altura);
			}
			else{
				// Sino el nodo lo toma un aux para poder verficarlo antes de eliminar
				aux = tmp;
				if(aux->der == NULL){
					tmp = aux->izq;
					altura = true;
				}
				else{
					// Si el nodo de la izquierda esta apuntando a nulo se debe apuntar al derecho
					if(aux->izq == NULL){
						tmp = aux->der;
						altura = true;
					}
					// Sino es nulo
					else{
						// Tomamos el nodo izquierdo
						aux1 = tmp->izq;
						altura = false;
						// Y mientras el nodo de la derecha no sea igual a nulo
						while(aux1->der != NULL){
							// Vamos rotando
							aux2 = aux1;
							aux1 = aux1->der;
							altura = true;
						}
						// Actualizamos el valor de la id 
						tmp->id = aux1->id;
						aux = aux1;
						if(altura == true){
							aux2->der = aux1->izq;
						}
						else{
							tmp->izq = aux1->izq;
						}
						// Debemos reestructurar el arbol
						reestructuraDer(tmp->izq, altura);
					}
				}
				delete aux;
			}
		}
	}
	else{
		cout << "El id no existe" << endl;
	}
			
}


// Imprimir el arbol en inorden
void ArbolAvl::inorden(Nodo *tmp){
	if(tmp != NULL){
		inorden(tmp->izq);
		cout << "[" << tmp->id << "]" << "->";
		inorden(tmp->der);
	}
};

