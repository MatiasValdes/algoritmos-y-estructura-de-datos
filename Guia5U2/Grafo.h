#ifndef GRAFO_H
#define GRAFO_H
#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;
// Incluimos la estructura del nodo
#include "Nodo.h"

// Creamos la clase grafo para la imagen
class Grafo{
    public:
        // Creamos la clase del grafo con el nodo raiz
        Grafo(Nodo* raiz);
        // Metodo que recorre nuestro arbol
        void recorrer_arbol(Nodo *raiz, ofstream &archivo);
};
#endif