#ifndef ARBOLAVL_H
#define ARBOLAVL_H
#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;
// Incluimos la estructura del nodo
#include "Nodo.h"

// Se define la clase arbol
class ArbolAvl{
    // Atributos privados de la clase
    private:
        // Generamos nodos que nos serán utiles dentro de la clase para no 
        // estar definiendolos en cada metodo, los hacemos parte de la clase
        Nodo *nodo1 = new Nodo;
        Nodo *nodo2 = new Nodo;
        Nodo *aux = NULL;
        Nodo *aux1 = NULL;
	    Nodo *aux2 = NULL;

    // Atributos publicos de la clase
    public:
        // Se define el constructor de la clase
        ArbolAvl();
        // Metodo para crear el nodo
        Nodo* crear_nodo(string dato);
        // Metodo que reestructura la izquierda y la derecha
        void reestructuraIzq(Nodo *&tmp, bool &altura);
        void reestructuraDer(Nodo *&tmp, bool &altura);
        // Metodo para buscar el nodo
        bool buscar_nodo(Nodo *tmp, string eliminar);
        // Metodo para insertar nodos
        void insertar_nodo(Nodo *&tmp, string dato, bool &altura);
        // Metodo para eliminar nodos
        void eliminar_nodo(Nodo *&tmp, string dato, bool &altura);
        // Metodo inorden
        void inorden(Nodo *tmp);
        
};
#endif