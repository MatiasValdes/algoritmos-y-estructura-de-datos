#include <iostream>
using namespace std;

#ifndef COORDENADA_H
#define COORDENADA_H

// Se define la clase coordenada
class Coordenada {
    // Atributos privados correspondientes a las posiciones
    private:
        float coor_x = 0;
        float coor_y = 0;
        float coor_z = 0;
    // Metodos publicos
    public:
        // Constructores tanto basico como por parametros
        Coordenada();
        Coordenada(float x, float y, float z);
        // Metodo set and get
        void set_x(float x);
        void set_y(float y);
        void set_z(float z);
        float get_x();
        float get_y();
        float get_z();
};
#endif
