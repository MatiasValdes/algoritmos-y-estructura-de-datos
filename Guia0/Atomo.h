#include <iostream>
using namespace std;
#include "Coordenada.h"
#ifndef ATOMO_H
#define ATOMO_H

// Se define la clase de atomo
class Atomo {
    // Atributos privados de la clase
    private:
        string nombre_atomo = "\0";
        int numero_atomo = 0;
        Coordenada coordenada;
    // Metodos publicos de la clase
    public:
        // Constructor
        Atomo(string nombre, int numero);
        // Metodos set and get
        void set_nombre(string nombre);
        void set_numero(int numero);
        string get_nombre();
        int get_numero();
        // Coordenada
        Coordenada get_coordenada();
        void set_coordenada(Coordenada coordenada);
};
#endif
