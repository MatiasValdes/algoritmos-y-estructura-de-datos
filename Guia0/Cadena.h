#include <iostream>
#include <list>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

// Se define la clase de cadena
class Cadena {
    // Atributos privados de la clase
    private:
        string letra;
        list<Aminoacido> aminoacidos;
    // Atributos publicos de la clase cadena
    public:
        // Constructor por parametros
        Cadena(string letra);
        // Metodo set and get
        void set_letra(string letra);
        string get_letra();
        // Metodos de agregar y obtener un aminoacido
        void add_aminoacido(Aminoacido aminoacido);
        list<Aminoacido> get_aminoacidos();
};
#endif
