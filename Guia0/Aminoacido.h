#include <iostream>
#include <list>
using namespace std;
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

// Se define la clase del aminoacido
class Aminoacido {
    // Atributos privados
    private:
        string nombre_aa = "\0";
        int numero_aa = 0;
        // Lista de atomos de un aminoacido
        list <Atomo> atomos;
    // Metodos publicos
    public:
        // Constructor por parametros
        Aminoacido(string nombre, int numero);
        // Metodos del aminoacido
        void set_nombre(string nombre);
        void set_numero(int numero);
        string get_nombre();
        int get_numero();
        // Metodos de la inserción del atomo
        void add_atomo(Atomo atomo);
        list<Atomo> get_atomos();
};
#endif
