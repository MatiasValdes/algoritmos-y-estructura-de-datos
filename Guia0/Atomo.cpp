#include <iostream>
using namespace std;
#include "Atomo.h"
#include "Coordenada.h"

// Constructor por parametros
Atomo::Atomo(string nombre, int numero){
    this->nombre_atomo = nombre;
    this->numero_atomo = numero;
    // Además se incluye la coordenada del atomo 
    this->coordenada = Coordenada();
}

// Metodos set and get

void Atomo::set_nombre(string nombre){
    this->nombre_atomo = nombre;
}

void Atomo::set_numero(int numero){
    this-> numero_atomo = numero;
}

string Atomo::get_nombre(){
    return this->nombre_atomo;
}

int Atomo::get_numero(){
    return this->numero_atomo;
}

// Agrega la coordenada al atomo
void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}

// Obtiene la cordenada del atomo
Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}
