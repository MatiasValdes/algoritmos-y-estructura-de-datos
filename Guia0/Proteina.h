#include <iostream>
#include <list>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

// Se define la clase proteina
class Proteina {
    // Atributos privados de la clase
    private:
        string nombre = "\0";
        string id = "\0";
        list <Cadena> cadenas;
    // Atributos publicos de la clase proteina
    public:
        // Constructor por parametros
        Proteina(string nombre, string id);
        // Metodos set and get
        void set_id(string id);
        void set_nombre(string nombre);
        string get_nombre();
        string get_id();
        // Metodos para agregar una cadena a la proteina 
        void add_cadena(Cadena cadena);
        list <Cadena> get_cadena();
};
#endif
