#include <iostream>
#include <list>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"


// Funcion del menú que nos retornará un valor para ser usado
int menu(){
    string opcion;
    // Opciones del menú
    cout << "¿Que es lo que desea hacer?" << endl;
    cout << "1-. Agregar proteína" << endl;
    cout << "2-. Imprimir proteínas" << endl;
    cout << "3-. Agregar 2 proteínas definidas por defecto" << endl;
    cout << "4-. Limpiar pantalla" << endl;
    cout << "5-. Salir del programa" << endl;
    getline(cin, opcion);
    // Se convierte a entero antes de retornarla
    return stoi(opcion);
}

// Funcion que nos permite imprimir los datos de la proteína en un orden
// legible para el usuario
void imprimir_datos_proteina(list<Proteina> proteinas){
    cout << "                           " <<  endl << endl;
    cout << "Las proteinas ingresadas son" << endl;
    for(Proteina i:proteinas){
        cout << "Nombre proteina: " << i.get_nombre() << endl;
        cout << "ID: " << i.get_id() << endl;
        for(Cadena c: i.get_cadena()){
            cout << "Cadena " << c.get_letra() << ":" << endl;
            for (Aminoacido a: c.get_aminoacidos()){
                cout << "  " << a.get_numero() << "-" << a.get_nombre() << ":" << endl;
                for(Atomo at: a.get_atomos()){
                    cout << "      \t" << at.get_numero() << "-." << at.get_nombre();
                    cout << "     Coordenadas ";
                    cout << "["<< at.get_coordenada().get_x() << ",";
                    cout << at.get_coordenada().get_y() << ",";
                    cout << at.get_coordenada().get_z() << "]" << endl;
                }
            }
        }
        cout << endl;
    }
    cout << endl << endl;
}


// Funcion que nos permite agregar dos proteínas definidas para uso del usuario
list<Proteina> agregar_datos(list<Proteina> proteinas){
      // Se crearan dos proteínas arbitrarias con motivo de opcion de usuario
      Proteina p1 = Proteina("Rubisco", "1IR2");
      Cadena p1_1 = Cadena("A");
      Aminoacido p1_aa = Aminoacido("ALA", 19);
      Atomo at_aa_1 = Atomo("N", 11);
      at_aa_1.set_coordenada(Coordenada (4.266, 69.075, 87.076));
      p1_aa.add_atomo(at_aa_1);
      p1_1.add_aminoacido(p1_aa);
      p1.add_cadena(p1_1);
      proteinas.push_back(p1);
      // Proteina 2
      Proteina p2 = Proteina("Galactosa transferasa", "1GWW");
      Cadena p2_1 = Cadena("A");
      Aminoacido p2_aa = Aminoacido("LYS", 2097);
      Atomo at_aa_2 = Atomo("O", 331);
      at_aa_2.set_coordenada(Coordenada(7.368, -8.643, 47.572));
      p2_aa.add_atomo(at_aa_2);
      p2_1.add_aminoacido(p2_aa);
      p2.add_cadena(p2_1);
      proteinas.push_back(p2);
      // Retornamos la lista con las proteinas que ingresamos
      return proteinas;
}


// Funcion principal
int main(int argc, char* argv[]){
    // Se inicializan variables que serán de utilidad dentro del codigo
    int opcion;
    int indicador=0, aux=0;
    string aminoacidos;
    string aa;
    string cadenas;
    string id;
    string nombre;
    string atomos;
    string letra;
    string elemento;
    string x, y, z;
    // Se crea la lista que almacenará todas las proteinas generadas
    list<Proteina> proteinas;
    // Limpiamos pantalla antes de iniciar el codigo
    system("clear");
    // Ciclo que nos permite mantener el ciclo infinitas veces
    while(indicador != 1){
        // Obtenemos la opción del menú
        opcion = menu();
        if (opcion == 1){
            // Se pide el nombre e id de la proteína en cuestión
            cout << "Ingrese el nombre de su proteina" << endl;
            getline(cin, nombre);
            cout << "¿Cual es la id? " << endl;
            getline(cin, id);
            // Creamos la proteína del usuario
            Proteina proteina = Proteina(nombre, id);
            cout << "¿Cuantas cadenas posee su proteina?" << endl;
            getline(cin, cadenas);
            for(int i=0;i<stoi(cadenas);i++){
                cout << "Ingrese la letra" << endl;
                getline(cin, letra);
                // Se crea la cadena sin aminoacidos
                Cadena cadena = Cadena(letra);
                cout << "¿Cuantos aminoacidos tiene la cadena?" << endl;
                getline(cin, aminoacidos);
                // Segun la cantidad de aminoacidos ingresados por el usuario
                // se generará el ciclo
                for(int j=0;j<stoi(aminoacidos);j++){
                    cout << "Nombre del aminoacido numero " << j+1 << "? ";
                    getline(cin, aa);
                    // Creamos el aminoacido
                    Aminoacido aminoacido = Aminoacido(aa, j+1);
                    cout << "Cuantos atomos tiene el aminoacido?  ";
                    getline(cin, atomos);
                    // Segun la cantidad de atomos, se genera el ciclo
                    for(int k=0;k<stoi(atomos);k++){
                        cout << "¿Nombre del atomo n°" << k+1 << "?  ";
                        getline(cin, elemento);
                        // Creamos el atomo
                        Atomo atomo = Atomo(elemento, k+1);
                        cout << "Coordenadas x,y,z respectivamente" << endl;
                        getline(cin, x);
                        getline(cin, y);
                        getline(cin, z);
                        // Colocamos las coordenadas del atomo además de
                        // agregar el atomo al aminoacido
                        atomo.set_coordenada(Coordenada(stof(x), stof(y), stof(z)));
                        aminoacido.add_atomo(atomo);
                    }
                    // Cuando ya posee todos sus atomos, se agrega el
                    // aminoacido a la cadena
                    cadena.add_aminoacido(aminoacido);
                }
                // Luego cuando todas las cadenas son creadas para la
                // proteina, estas cadenas son agregadas a la proteina
                proteina.add_cadena(cadena);
              }
              // Cuando la proteína tiene todos los elementos, ésta se
              // agrega a la lista de proteinas
              proteinas.push_back(proteina);
        }
        else if(opcion == 2){
            // Primero nos aseguraremos que la lista de proteinas contenga
            // algun dato
            if(proteinas.size() > 0){
                // Si contiene datos llamamos a la funcion para imprimir las proteinas
                imprimir_datos_proteina(proteinas);

            }
            else{
                // Ofrecemos la opción de agregar proteinas
                cout << "No se han agregado datos para poder mostrar " << endl;
                cout << "Si no desea agregar ninguno, le ofrecemos agregar";
                cout << " proteinas por defecto con la opcion 3" << endl;
                system("sleep 7");
            }
        }
        else if(opcion == 3){
            // Se agregan dos proteinas ingresadas pero antes nos aseguraremos
            // de que no las haya agregado antes utilizando una auxiliar
            if(aux== 0){
                cout << "Se agregarán 2 proteinas definidas" << endl;
                proteinas = agregar_datos(proteinas);
                // Se realizan sleeps para asimilar que el programa se está
                // ejecutando y que no salga nada en el menú directamente
                system("sleep 1");
                cout << "Agregadas exitosamente" << endl << endl;
                system("sleep 2");
                aux = 1;
            }
            else{
                cout << "Ud ya agregó las proteínas que venian por defecto" << endl << endl;
                system("sleep 2");
            }
        }
        else if(opcion ==4){
            // Limpiamos la pantalla de terminal para hacer mas legible las opciones
            system("clear");
        }
        else if(opcion == 5){
            // Cerramos el ciclo while cambiando el valor del indicador
            cout << "Gracias por utilizar este programa :D" << endl;
            indicador = 1;
        }
        else{
            system("sleep 3");
            cout << "Opcion no valida" << endl;
            opcion = menu();
        }
    }
    return 0;
}
