#include <iostream>
using namespace std;
#include "Coordenada.h"

// Creacion de la clase coordenada por dos constructores, en ambos se
// instancian las coordenadas x,y,z
// Constructor basico
Coordenada::Coordenada(){
    float coor_x = 0;
    float coor_y = 0;
    float coor_z = 0;
}

// Constructor por parametros
Coordenada::Coordenada(float x, float y, float z){
    this->coor_x = x;
    this->coor_y = y;
    this->coor_z = z;
}

// Metodos set and get de las coordenadas
void Coordenada::set_x(float x){
    this->coor_x = x;
}

void Coordenada::set_y(float y){
    this->coor_y = y;
}

void Coordenada::set_z(float z){
    this->coor_z = z;
}

float Coordenada::get_x(){
  return this->coor_x;
}

float Coordenada::get_y(){
    return this->coor_y;
}

float Coordenada::get_z(){
    return this->coor_z;
}
