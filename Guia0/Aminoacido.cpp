#include <iostream>
#include <list>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"

// Constructor por parametros
Aminoacido::Aminoacido(string nombre, int numero){
    this->nombre_aa = nombre;
    this->numero_aa = numero;
    list <string> Atomo;
}

// Metodos set and get

void Aminoacido::set_nombre(string nombre){
    this->nombre_aa = nombre;
}

void Aminoacido::set_numero(int numero){
    this->numero_aa = numero;
}

string Aminoacido::get_nombre(){
    return this->nombre_aa;
}

int Aminoacido::get_numero(){
    return this->numero_aa;
}

// Agrega atomos a los aminoacidos
void Aminoacido::add_atomo(Atomo atomo){
    this->atomos.push_back(atomo);
}

// Obtiene los atomos del aminoacido
list <Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}
