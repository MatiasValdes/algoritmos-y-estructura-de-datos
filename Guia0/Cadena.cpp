#include <iostream>
#include <list>
using namespace std;
#include "Cadena.h"
#include "Aminoacido.h"

// Constructor por parametros
Cadena::Cadena(string letra){
    this->letra = letra;
    list <string> aminoacidos;
}

// Metodo set and get

void Cadena::set_letra(string letra){
    this->letra = letra;
}

string Cadena::get_letra(){
    return this->letra;
}

// Metodo que agrega aminoacidos a la cadena
void Cadena::add_aminoacido(Aminoacido aminoacido){
    this->aminoacidos.push_back(aminoacido);
}

// Metodo que obtiene la lista de aminoacidos de la cadena
list<Aminoacido> Cadena::get_aminoacidos(){
    return this->aminoacidos;
}
