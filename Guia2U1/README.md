# Guia2 Unidad 1: Puerto seco 

## Empezando
El programa simula un puerto seco, en donde el usuario podrá definir todas las dimensiones del mismo, agregar contenedores con nombre y numero que los identifique para que si el usuario desea eliminarlos sea más fácil identificarlos.


## Pre-requisitos

* Sistema operativo Linux
* Compilador make

### Construido con
* Ubuntu: sistema operativo
* Lenguaje de programación: C++
* Atom: Editor de codigo

## Compilación del programa

1) Dentro de la terminal ingresar a la direccion donde esta el programa a ejecutar, por ejemplo:
2) Cuando nos encontremos en la dirección ejecutaremos el comando: "make"
3) Luego debemos utilizar el ejecutable de la siguiente forma: ./ejercicio
4) Utilizar el programa

## Explicación del programa

* Inicialmente el programa le pedirá al usuario que defina la cantidad de columnas del puerto seco, posteriormente el usuario deberá definir la cantidad máxima de cada fila [pila].
* Luego se desplegará un menú en el que el usuario obtendrá cuatro opciones:
1) Agregar contenedor
2) Eliminar contenedor
3) Visualizar puerto seco
4) Salir del programa
Dependiendo de la que elija el programa ejecutará las acciones
* Si desea agregar contenedores, el programa le mostrará como se encuentra el puerto seco y el usuario deberá especificar en que columna desea agregar el contenedor, si es posible el programa le indicará que agrege tanto el nombre como el numero para el contenedor
* Si desea eliminar un contenedor, deberá especificar la columna y los datos del contenedor, si se encuentran contenedores arriba del deseado, estos se moverán a otra columna
* Si desea visualizar el puerto seco, se desplegará como se encuentra el puerto
* La ultima opcion, solamente saldrá del programa

# Muchas gracias por utilizar el programa
