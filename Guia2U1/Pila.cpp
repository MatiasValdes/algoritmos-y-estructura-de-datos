#include <iostream>
using namespace std;
#include "Pila.h"
#include "Contenedor.h"

// Constructor
Pila::Pila(){
    // Atributos de la clase
    this->largo = 0;
    this->tope = -1;
    this->contenedores = NULL;
    this->max = 0;
    bool band = false;
    this->band = band;
}

// Constructor con el largo
Pila::Pila(int largo){
    // Atributos de la clase
    this->largo = largo;
    this->tope = -1;
    this->contenedores = NULL;
    this->max = 0;
    bool band = false;
    this->band = band;
}

// Metodo en el que se crea la pila
void Pila::crear_pila(int largo){
    // Se define el maximo
    this->max = largo - 1;
    // Creamos el arreglo de contenedores
    this->contenedores = new Contenedor[largo];

}

// Metodo para observar si la pila se encuentra llena
void Pila::Pila_llena(){
    // Si el tope con el max son iguales es porque la pila se encuentra llena
    if(this->tope == this->max){
        // Definimos el booleano como verdadero
        this->band = true;
    }
    // Sino es así es porque aun no esta llena y el booleano será falso
    else{
        this->band = false;
    }
}

// Metodo para ver si la pila se encuentra vacía
void Pila::Pila_vacia(){
    // Si el tope tiene un valor negativo [-1] es porque está vacía
    if(this->tope == -1){
        this->band = true;
    }
    else{
        this->band = false;
    }
}

// Metodo para agregar un dato a la pila
void Pila::pushContenedor(Contenedor contenedor){
    // Primero debemos asegurarnos de que la pila no esté llena lo que nos hará
    // que cambie el valor del booleano dependiendo de la pila
    Pila_llena();
    // Si el booleano es falso es porque no está llena
    if(this->band == false){
        // Aumentamos el tope
        this->tope = this->tope + 1;
        // Agregamos el dato que es de tipo contenedor
        this->contenedores[this->tope] = contenedor;
    }
    // Si el booleano es verdadero es porque esta llena
    else{
        cout << "No es posible agregar en esta columna" << endl;
    }
}

// Metodo para eliminar datos de la pila
Contenedor Pila::popContenedor(){
    // Primero nos aseguramos que la pila no esté vacia osino no se pueden
    // eliminar datos
    Pila_vacia();
    // Generamos un contenedor temporal para almacenar el contenedor a extraer
    Contenedor contenedorTemp = Contenedor();
    // Si el bool es falso podemos eliminar datos
    if(this->band == false){
        // Se almacena el contenedor a extraer
        contenedorTemp = this->contenedores[this->tope];
        // Se crea un nuevo objeto de contenedor
        Contenedor nuevo = Contenedor();
        this->contenedores[this->tope] = nuevo;


        // Reducimos el tope
        this->tope = this->tope - 1;
        cout << endl;
        // Se devuelve el contenedor para evualuarlo si es el deseado
        return contenedorTemp;
    }
    else{
        cout << "Columna vacia" << endl;
    }
}

// Metodo para imprimir la pila y traemos el dato del iterador
void Pila::imprimirContenedor(int i){
    // Buscamos la forma más optima de imprimir
    cout << "[ " << this->contenedores[i].get_NombreEmpresa();
    if(this->contenedores[i].get_NumeroContenedor() != 0){
        cout << " / " << this->contenedores[i].get_NumeroContenedor() << "  \t";
      }
      cout << " ]\t";
}


// Metodo para obtener el booleano de la banda que nos servirá cuando debamos
// eliminar un dato y ver si es posible con el booleano de la pila
bool Pila::get_band(){
    return this->band;
}
