#include <iostream>
using namespace std;
#include "Contenedor.h"

#ifndef PILA_H
#define PILA_H

// Se define la clase pila
class Pila{
    // Atributos privados
    private:
        // Se definen los atributos privados de la clase
        int largo = 0;
        int tope = -1;
        Contenedor *contenedores;
        int max = 0;
        bool band = false;
    // Atributos publicos que corresponden a los metodos
    public:
        // Constructores
        Pila();
        Pila(int largo);
        // Metodo para crear pila y metodos para observar si la pila esta vacia
        // o está llena, además de un metodo que nos entrega el booleano de la
        // banda
        void crear_pila(int largo);
        void Pila_vacia();
        void Pila_llena();
        // Metodo para agregar, remover e imprimir los contenedores en las
        // pilas
        void pushContenedor(Contenedor contenedor);
        Contenedor popContenedor();
        void imprimirContenedor(int i);
        bool get_band();
};
#endif
