#include <iostream>
using namespace std;


#ifndef CONTENEDOR_H
#define CONTENEDOR_H

// Se crea la clase contenedor
class Contenedor{
      // Se definen los atributos privados de la clase
      private:
          string nombreEmpresa;
          int numeroContenedor;

      // Se definen los atributos publicos de la clase
      public:
          // Ambos constructores
          Contenedor();
          Contenedor(int numero);
          // Metodos para setear el nombre y el numero
          void set_NombreEmpresa(string nombre);
          void set_NumeroContenedor(int numero);
          // Metodos para obtener ambos datos
          string get_NombreEmpresa();
          int get_NumeroContenedor();

};
#endif
