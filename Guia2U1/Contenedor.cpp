#include <iostream>
using namespace std;
#include "Contenedor.h"

// Constructor por defecto
Contenedor::Contenedor(){
    this->nombreEmpresa = " Disponible ";
    this->numeroContenedor = 0;

}

// Constructor por el parametro del numero
Contenedor::Contenedor(int numero){
    this->nombreEmpresa = " Disponible ";
    this->numeroContenedor = numero;
}

// Metodos para setear tanto el nombre como el numero del contenedor
void Contenedor::set_NombreEmpresa(string nombre){
    this->nombreEmpresa = nombre;
}

void Contenedor::set_NumeroContenedor(int numero){
    this->numeroContenedor = numero;
}

// Metodos para obtener el nombre de la empresa y el numero del contenedor
string Contenedor::get_NombreEmpresa(){
    return this->nombreEmpresa;
}

int Contenedor::get_NumeroContenedor(){
    return this->numeroContenedor;
}
