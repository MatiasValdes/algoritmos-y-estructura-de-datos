#include <iostream>
using namespace std;
#include "Pila.h"
#include "Contenedor.h"

// Funcion que nos permitirá imprimir el puerto seco
void imprimir_puertoSeco(int columnas, Pila *puerto_seco, int tamanio){
    // Se imprime el puerto seco
    cout << "------------------------------------------------------" << endl;
    cout << "-------------------- PUERTO SECO ---------------------" << endl;
    // Se recorren las pilas
    for(int i=(tamanio-1);i>=0;i--){
        for(int j=0;j<columnas;j++){
            // Imprimimos cada contenedor de la pila [puerto seco]
            puerto_seco[j].imprimirContenedor(i);
        }
        cout << endl;
    }
    cout << endl;
    // Realizamos un ciclo debajo de las pilas para denotar las columnas para el usuario
    for(int i=0; i<columnas; i++){
		     cout << "   -COLUMNA " << i+1 << "-   \t  ";
	   }
     cout << endl;
}

// Funcion que nos devolverá una opcion del usuario
int menu(){
    string opcion;
    // Se imprimen las opciones del usuario
    cout << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "Ingrese una de las siguientes opciones" << endl;
    cout << "[1] Agregar contenedor" << endl;
    cout << "[2] Remover contenedor" << endl;
    cout << "[3] Ver contenedor" << endl;
    cout << "[4] Salir " << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "¿Que desea hacer?  ";
    // Capturamos la opcion y la devolvemos en formato numerico
    getline(cin, opcion);
    return stoi(opcion);

}

// Funcion principal
int main(int argc, char* argv[]){
    // Variables que almacenaran datos del usuario
    string line;
    int opcion = 0;
    int tamanio=0, columnas=0;
    // Variables que se usarán cada vez que el usuario ingrese un contenedor
    string nombre;
    int numero=0, iterador=0;
    // Contenedor que nos permitirá realizar los cambios de los contenedores
    Contenedor container = Contenedor();

    // Pedimos la cantidad de columnas para el puerto seco
    cout << "Ingrese la cantidad de columnas para los contenedores" << endl;
    getline(cin,line);
    columnas = stoi(line);
    // Generamos un puerto seco con las columnas ingresadas
    Pila *puerto_seco = new Pila[columnas];
    // Pedimos la cantidad máxima de datos para cada pila
    cout << "Ingrese la capacidad de cada columna para sus contenedores" << endl;
    getline(cin, line);
    tamanio = stoi(line);

    // Realizaremos un ciclo para agregar tantas pilas como columnas hayan sido
    // requeridas por el usuario
    for(int i=0;i<columnas;i++){
        // Le enviamos el tamaño máximo de datos para crear las pilas
        puerto_seco[i].crear_pila(tamanio);
    }
    int indicador = 0;
    // Ciclo para mantener el ciclo
    while(opcion != 10){
        indicador = 0;
        // Capturamos la opcion del menú
        opcion = menu();
        // Agregar un contenedor
        if(opcion == 1){
            // Le mostramos al usuario como se encuentra el puerto antes de
            // que vaya a agregar algo
            imprimir_puertoSeco(columnas, puerto_seco, tamanio);
            // Pedimos la columna donde quiere ingresar el contenedor
            cout << "Ingrese la columna donde agregará el contenedor" << endl;
            getline(cin, line);
            // Como en el menú imprimimos con un +1 debemos restarlo ahora
            iterador = stoi(line) - 1;
            // Pedimos el nombre del contenedor
            cout << "Ingrese el nombre de la empresa" << endl;
            getline(cin, line);
            nombre = line;
            // Seteamos el nombre del contenedor
            container.set_NombreEmpresa(nombre);
            // Pedimos el numero del contenedor
            cout << "Ingrese el numero del contenedor" << endl;
            getline(cin, line);
            numero = stoi(line);
            // Seteamos el numero del contenedor
            container.set_NumeroContenedor(numero);
            // Agregamos el contenedor que quiso agregar el usuario al puerto seco
            puerto_seco[iterador].pushContenedor(container);
        }
        // Remover un contenedor
        else if(opcion == 2){
            // Igual como lo hacemos para agregar, debemos mostrar como se encuentra
            // nuestro puerto para que el usuario pueda elegir
            imprimir_puertoSeco(columnas, puerto_seco, tamanio);
            // Pedimos datos del contenedor a extraer
            cout << "Ingrese la columna del contenedor a extraer" << endl;
            getline(cin, line);
            iterador = stoi(line) - 1;
            // Pedimos los datos del contenedor a remover, deben ser iguales
            cout << "Nombre del contenedor" << endl;
            getline(cin,line);
            nombre = line;
            cout << "Numero del contenedor" << endl;
            getline(cin,line);
            numero = stoi(line);
            // Debemos realizar un ciclo para buscar el contenedor apropiado
            // Lo hacemos verdadero hasta que se pare cuando termine de realizar los procesos
            while(indicador != 1){
                // Realizamos un pop del contenedor
                container = puerto_seco[iterador].popContenedor();
                // Debemos verificar si el contenedor es el indicado
                // Si es así solo mostramos como quedó nuestro contenedor
                if(container.get_NombreEmpresa() == nombre && container.get_NumeroContenedor() == numero){
                    imprimir_puertoSeco(columnas, puerto_seco, tamanio);
                    indicador  = 1;
                }
                // Debemos ir moviendo los contenedores de la columna deseada hasta que
                // se encuentre el contenedor deseado
                else{
                  // Realizamos un ciclo por las columnas del puerto seco
                  for(int i=0; i<columnas; i++){
                      // Tenemos la idea de que el contenedor que está por encima del solicitado
                      // debe agregarse en otra pila (columna)
                      if(i!=iterador){
                        //Vemos si esta llena la pila
                        puerto_seco[i].Pila_llena();
                        // Vemos si la pila se encuentra llena
                        if(puerto_seco[i].get_band() == true){
                            // Se agrega nuevamente a la posicion que se elimino el contenedor
                            // si es que la pila se encuentra llena
                            puerto_seco[iterador].pushContenedor(container);
                            cout << "┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼" << endl;
                            cout << endl;
                            cout << "No es posible mover los contenedores" << endl;
                            cout << endl;
                            cout << "┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼" << endl;
                            indicador = 1;
                            break;
                        }
                        // Si la pila no se encuentra llena es posible agregar un contenedor
                        // y así mantenerlos
                        if(puerto_seco[i].get_band() == false){
                              // Se agrega el contenedor en la nueva columna
                              puerto_seco[i].pushContenedor(container);
                              //Luego de cambiarla de posición imprimimos como va el puerto
                              imprimir_puertoSeco(columnas, puerto_seco, tamanio);
                              // Realizamos un break o en el siguiente ciclo nos cambiará nuevamente
                              // el contenedor que ya se debió haber eliminado
                              break;
                          }
                       }
                    }
                }
            }
        }
        // Solo imprimimos el puerto
        else if(opcion == 3){
            imprimir_puertoSeco(columnas, puerto_seco, tamanio);
        }
        // Finalizacion del programa
        else if(opcion == 4){
            cout << "Fin del programa" << endl;
            opcion = 10;
        }
        // Si se entrega una opcion no valida numerica
        else{
            cout << "Ingrese una opcion valida" << endl;
        }
    }
    return 0;
}
