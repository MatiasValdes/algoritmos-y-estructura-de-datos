#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

using namespace std;

// Constructor por defecto de la clase arbol
Arbol::Arbol(){}

// Metodo para obtener raiz del arbol
Nodo* Arbol::get_raiz(){
	return this->raiz;
}

//Metodo para crear nodo
void Arbol::crear_nodo(int numero){
	Nodo *tmp = new Nodo;
	// Se asigna la info al nodo que se desea guardar
	tmp->numero = numero;
    // Por defecto, la izquierda y la derecha apuntarán a nulo
	tmp->izq = NULL;
	tmp->der = NULL;
			
	// Ahora insertamos el nodo a la raíz
	this->raiz = insertar_nodo(this->raiz, tmp);
}

// Metodo para buscar el nodo lo que nos entregará un true o un false
bool Arbol::buscar_nodo(Nodo *tmp, int eliminar){
	// Se empezará a buscar el nodo
	// Si el nodo es menor por la izquierda, se busca por izq
	if(eliminar < tmp->numero){
		// Si el nodo está apuntando a nulo es porque no existe el nodo
		if(tmp->izq == NULL){
			cout << "No está el numero" << endl;
			return false;
		}
		else{
			// Si no es nulo es porque quizás existe el nodo por lo tanto
			// se debe seguir buscando por izquierda
			buscar_nodo(tmp->izq, eliminar);
		}
	}
	// Si el numero no es menor por la izquierda, será mayor por derecha
	else{
		// Nos aseguraremos de que el numero sea mayor y se busque
		if(eliminar > tmp->numero){
			// Seguimos el mismo procedimiento que por izquierda
			if(tmp->der == NULL){
				cout << "No está el numero" << endl;
				return false;
			}
			else{
				buscar_nodo(tmp->der, eliminar);
			}
		} // Pero si no es ni mayor por derecha ni por izquierda nos da a pensar que es el mismo numero
		else{
			cout << "Se encontró el numero a eliminar" << endl;
			// this->raiz = eliminar_nodo(this->raiz, eliminar);
			return true;
		}
	}
}

// Metodo para insertar nodo
Nodo* Arbol::insertar_nodo(Nodo *tmp, Nodo *nuevo){
	// Si el nodo tmp está vacio se debe agregar el nodo en esa posición
	if(tmp == NULL){
		tmp = nuevo;
		cout << "Se agregó el dato con exito" << endl << endl;
	}
	// Si es que no está vacio
	else{
		// Vemos si el valor es menor al de la raiz 
		if(nuevo->numero < tmp->numero){
			tmp->izq = insertar_nodo(tmp->izq, nuevo);
		}
		// Sino se agrega por la derecha
		else if(nuevo->numero > tmp->numero){
			tmp->der = insertar_nodo(tmp->der, nuevo);
		}
		// Osino son iguales y no se pueden agregar datos repetidos
		else{
			cout << "Numero repetido, ingresar un nuevo numero" << endl << endl;
		}
	}
	return tmp;
}

// Metodo que busca el valor minimo
Nodo* Arbol::valor_minimo(Nodo *tmp){
	// Se crea un nodo con el nodo que trae la función para buscar
	Nodo *minimo_valor = tmp;
	// Ciclo while mientras el nodo 
	while(minimo_valor->izq != NULL){
		// Se sigue buscando el valor minimo por la izquierda
		minimo_valor = minimo_valor->izq;
	}
	// retorna el nodo con el nodo con el valor minimo
	return minimo_valor;
}

// Metodo para eliminar un nodo
Nodo* Arbol::eliminar_nodo(Nodo *tmp, int numero, bool &altura){
	// Se crean 2 nodos auxiliares y un booleano como indicador
	Nodo *aux;
	Nodo *aux1;
	bool indicador;
	// Si el nodo apunta al nulo
	if(tmp == NULL){
		// Solo retornamos el nodo
		return tmp;
	}
	// Si el numero que deseamos eliminar es menor al dato del nodo actual
	// se busca por izquierda
	if(numero < tmp->numero){
		// Se hace una busqueda recursiva por la izquierda
		tmp->izq = eliminar_nodo(tmp->izq, numero);
	}
	// Si el numero es mayor al que posee el dato se busca por derecha
	else if(numero > tmp->numero){
		// Se hace una busqueda recursiva por la derecha
		tmp->der = eliminar_nodo(tmp->der, numero);
	}
	else{
		// Si el nodo de la izquierda apunta a nulo
		if(tmp->izq == NULL){
			// Se guarda el nodo de la derecha y se elimina el temporal
			aux = tmp->der;
			delete(tmp);
			// Devolvemos el nodo que tenemos guardado en aux
			return aux;
		}
		// En cambio si el de la derecha apunta a nulo
		else if(tmp->der == NULL){
			// Se guarda el de la izquierda y se elimina el nodo
			aux = tmp->izq;
			delete(tmp);
			// Se retorna el nodo de la izquierda
			return aux;
		}
		// Igualamos el auxiliar al valor minimo que se encuentra
		aux = valor_minimo(tmp->der);
		// El nodo se igualará al dato que tiene el auxiliar
		tmp->numero = aux->numero;
		// Por el nodo de la derecha se empieza la recursividad con el dato a eliminar que posee el aux
		// que es el mismo que quería eliminar el usuario
		tmp->der = eliminar_nodo(tmp->der, aux->numero);
	}
	return tmp;
			
}

// Imprimir el arbol en preorden 
void Arbol::preorden(Nodo *tmp){
	if(tmp != NULL){
		cout << "[" << tmp->numero << "]" << "->";
		preorden(tmp->izq);
		preorden(tmp->der);
	}
}

// Imprimir el arbol en inorden
void ArbolAvl::inorden(Nodo *tmp){
	if(tmp != NULL){
		inorden(tmp->izq);
		cout << "[" << tmp->numero << "]" << "->";
		inorden(tmp->der);
	}
}

// Imprimir el arbol en postorden
void Arbol::postorden(Nodo *tmp){
	if(tmp != NULL){
		postorden(tmp->izq);
		postorden(tmp->der);
		cout << "[" << tmp->numero << "]" << "->";
	}
}

// Metodo para crear el grafico del arbol
void Arbol::crear_grafico(Nodo *tmp){
	//creacion de archivo.txt
	ofstream archivo("grafo.txt", ios::out);
	// Primero debemos intentar abrir el archivo para evitar errores
	if (archivo.fail()){
		cout << "**No se pudo abrir archivo**" << endl;
	}
	else{
		// Escritura en el archivo del arbol
		archivo << "digraph G {\n";
		archivo << "node [style=filled fillcolor=red] ;\n";
		// llamada a metodo para ingresar informacion del arbol
		recorrer_arbol(tmp, archivo);	
		archivo << "}";
		archivo.close();
		// Generacion de la imagen a traves del archivo de texto
		system("dot -Tpng -ografo.png grafo.txt &");
		// Apertura de la imagen
		system("eog grafo.png &");
	}	
}

// Funcion que recorre el arbol y crea el archivo.txt
void Arbol::recorrer_arbol(Nodo *tmp, ofstream &archivo){
	// Recorrido del arbol en preorden y se agregan datos al archivo.txt
	if(tmp != NULL){
		if (tmp->izq != NULL){
			archivo << tmp->numero << "->" << tmp->izq->numero << ";\n";
		} 
		else{      
			archivo << '"' << tmp->numero << "i" << '"' << " [shape=point];\n";
			archivo << tmp->numero << "->" << '"' << tmp->numero << "i" << '"' << ";\n" ;
		}
		if (tmp->der != NULL) {
			archivo << tmp->numero << "->" << tmp->der->numero << ";\n";
		} 
		else {
			archivo << '"' << tmp->numero << "d" << '"' << " [shape=point];\n";
			archivo << tmp->numero << "->" << '"' << tmp->numero << "d" << '"' <<";\n";
		}
		// Luego se recorre en izquierda y derecha
		recorrer_arbol(tmp->izq, archivo);
		recorrer_arbol(tmp->der, archivo);
	}
};	