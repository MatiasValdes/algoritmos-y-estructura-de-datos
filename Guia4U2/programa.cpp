#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

// Funcion del menú
int menu(){
    string opcion;
    // Se imprimen las opciones del usuario
    cout << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "Ingrese una de las siguientes opciones" << endl;
    cout << "[1] Insertar numero al arbol" << endl;
    cout << "[2] Eliminar numero del arbol" << endl;
    cout << "[3] Modificar un elemento del arbol" << endl;
    cout << "[4] Mostrar contenido del arbol " << endl;
    cout << "[5] Generar el grafo" << endl;
    cout << "[6] Salir del programa" << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "¿Que desea hacer?  ";
    // Capturamos la opcion y la devolvemos en formato numerico
    getline(cin, opcion);
    return stoi(opcion);

}

// Funcion principal
int main(int argc, char* argv[]){
    // Variables de apoyo para el codigo
    int opcion=1;
    string line;
    int eliminar = 0;
    int numero = 1;
    // Creamos un nodo para utilizarlo para capturar la raiz
    Nodo *raiz = new Nodo();
    // Y creamos nuestro arbol binario
    Arbol *arbolbin = new Arbol();

    // Ciclo para mantener el ciclo
    while(opcion != 0){
        // Capturamos la opcion del menú
        opcion = menu();
        system("clear");
        // Opcion 1 para agregar un elemento
        if(opcion == 1){
            cout << "Para dejar de agregar numeros, ingrese el 0" << endl;
            // Ciclo while mientras el numero ingresado no sea un 0 
            while(numero != 0){
                cout << "Numero a agregar: ";
                getline(cin, line);
                numero = stoi(line);
                // Mientras el numero no sea 0, crearemos un nodo en nuestro arbol binario
                if(numero != 0){
                    arbolbin->crear_nodo(numero);
                }
            }
        }
        // Opcion para eliminar un elemento
        else if(opcion == 2){
            raiz = arbolbin->get_raiz();
            // Nos aseguramos que el arbol no se encuentre vacio
            if(raiz != NULL){
                // Mostramos los elementos que contiene el arbol
                cout << "A continuación se mostrarán los nodos del arbol para elegir el que se desea eliminar" << endl;
                arbolbin->inorden(raiz);
                cout << endl;
                // Pedimos el dato a eliminar
                cout << "Ingrese el numero a eliminar: "; 
                getline(cin, line);
                eliminar = stoi(line);
                // Se llama la funcion de busqueda del nodo que despues deriva en la eliminacion
                arbolbin->buscar_nodo(raiz, eliminar);
                eliminar = 0;
            }
            else{
                cout << "El arbol se encuentra vacío" << endl << endl;
            }
        }
        // Modificar un elemento del arbol
        else if(opcion == 3){
            raiz = arbolbin->get_raiz();
            // Nos aseguramos que el arbol no se encuentre vacio
            if(raiz != NULL){
                // Mostramos el arbol como se encuentra actualmente
                cout << "A continuación se mostrarán los nodos del arbol para elegir el que se desea eliminar" << endl;
                arbolbin->inorden(raiz);
                cout << endl;
                // Pedimos el numero a editar y el numero que se actualizara en esa posicion
                cout << "Ingrese el numero a editar: "; 
                getline(cin, line);
                eliminar = stoi(line);
                cout << "Ingrese el numero que querrá actualizar en esa posicion: "; 
                getline(cin, line);
                numero = stoi(line);
                // Si el numero se encuentra y además se elimino
                if(arbolbin->buscar_nodo(raiz, eliminar) == true){
                    // Se crea el nuevo nodo
                    arbolbin->crear_nodo(numero);
                    cout << "Se ha generado el cambio que ud necesitaba" << endl;
                }
            }
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
        }
        // Mostrar el contenido del arbol
        else if(opcion == 4){
            raiz = arbolbin->get_raiz();
            // Nos aseguraremos de que no este vacio
            if(raiz != NULL){
                // Se mostraran todas las posibles de recorrido del arbol
                cout << "-------- ARBOL ACTUAL --------" << endl << endl;
                cout << "----- METODO INORDEN -----" << endl;
                arbolbin->inorden(raiz);
                cout << endl;
                cout << "----- METODO POSTORDEN -----" << endl;
                arbolbin->postorden(raiz);
                cout << endl;
                cout << "----- METODO PREORDEN -----" << endl;
                arbolbin->preorden(raiz);
                cout << endl;
            }
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
        }
        // Crear el grafico del arbol
        else if(opcion == 5){
            // Capturamos la raiz del arbol
            raiz = arbolbin->get_raiz();
            // Si el arbol no se encuentra vacio crearemos un grafico
            if(raiz != NULL){
                // Se llama al metodo de la clase arbol para crear el grafo
                arbolbin->crear_grafico(raiz);
            }
            // Sino solo se pasa
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
        }
        // Opcion para salir del programa
        else if(opcion == 6){
            cout << "Gracias por utilizar el programa" << endl;
            return 0;
        }
        // Si se entrega una opcion no valida numerica
        else{
            cout << "Ingrese una opcion valida" << endl;
        }
    }
    return 0;
}