# Guia Numero 4 unidad II

## Descripcion del programa 
El programa cuenta con la creación de un arbol binario, en el cual el usuario puede agregar elementos, eliminar elementos, modificar elementos, ver como el arbol se escribe en diferentes ordenes (postorden,inorden y preorden) y además cuenta con la opción de generar un grafo con su arbol en formato png

## Problemas en el programa
Hubo un problema al generar la eliminación de un nodo ya que se eliminaban todos los nodos de la derecha correspondiente al que se deseaba eliminar, luego de probar de otras formas y recurriendo al libro de la bibliografía se genero otro problema que generaba la inserción pero creaba unos nodos en vacío hasta que finalmente se llegó a la solución que lo unico que se debía hacer con respecto a los demás era cambiar el lado de la función de obtener el dato de la derecha para el analisis.Además se anticipó un posible problema con los headers que se obtuvo en trabajos anteriores por lo tanto se decidió inmediatamente agregar la estructura del nodo al arbol.h que era donde se debía utilizar para así evitar problemas en la ejecución del programa.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
* Paquete graphviz

En caso de tener los requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

### ¿Como instalar graphviz?
Para instalar graphviz, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install graphviz
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el paquete. Una vez termine la descarga, el paquete estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./programa
```

## Funcionamiento del programa
El programa iniciará mostrando un menú el cual es el siguiente
* [1] Insertar numero al arbol
* [2] Eliminar numero del arbol
* [3] Modificar un elemento del arbol
* [4] Mostrar contenido del arbol
* [5] Generar el grafo
* [6] Salir del programa



Dependiendo de la opción que elija el programa hará las cosas.


### Opción 1
El programa empezará a agregar números hasta que se le indique con el valor 0 que se dejen de agregar numeros, en este inciso el programa no dejará agregar numeros repetidos ya que deben ser unicos

### Opcion 2
El programa mostrará el arbol actual y el usuario solo debe ingresar el valor del numero que desea eliminar

### Opcion 3
El programa le solicitará el numero para editar y luego pedirá el dato que quiere que sea agregado. En este punto el programa eliminará y luego agregara el dato

### Opcion 4
Cuando el usuario ingrese la opcion se mostrara el arbol en diferentes recorridos en la terminal

### Opcion 5
El programa generara una imagen con el arbol actual que el usuario haya creado, esta imagen será en formato png

### Opcion 6
El programa simplemente finalizará por lo que se recomienda generar una imagen antes de realizar esta opción.

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl