#include <iostream>
using namespace std;

// Clases
#include "Lista.h"

// Funcion que nos imprime ambas listas en cualquier momento
void impresion_listas(Lista *lista1, Lista *lista2){
    cout << "----------------------------------------------------------------" << endl;
    cout << "Su lista numero 1 actualmente se encuentra de la siguiente forma" << endl;
    lista1->imprimir();
    cout << "----------------------------------------------------------------" << endl;
    cout << "Su lista numero 2 actualmente se encuentra de la siguiente forma" << endl;
    lista2->imprimir();
}

// Funcion principal
int main(int argc, char* argv[]){
    // Iniciamoslas listas
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();
    Lista *lista3 = new Lista();
    // Variables de apoyo al codigo
    string line;
    int opcion=0, numero=0, indicador=40, tam1=0, tam2=0;
    // Preferí pedir datos ya que si generaba un ciclo, podría dejar un numero que el usuario
    // quisiera ingresar y que yo lo restringí
    cout << "Ingrese el tamaño de la lista 1: ";
    getline(cin, line);
    tam1 = stoi(line);
    cout << "Ingrese el tamaño de la lista 2: ";
    getline(cin, line);
    tam2 = stoi(line);

    // Agregamos los datos a la lista 1 segun el tamaño del usuario
    cout << "------ Agregando datos a Lista 1 ------" << endl;
    for(int i=0; i<tam1; i++){
        // Agregamos los numeros
        cout << "Ingrese el numero a agregar: ";
        getline(cin, line);
        numero = stoi(line);
        // Creamos los nodos
        lista1->crear(numero);
    }

    // Agregamos los datos a la lista 2 según el tamaño del usuario
    cout << "------ Agregando datos a Lista 2 ------" << endl;
    for(int i=0; i<tam2; i++){
        // Agregamos los numeros
        cout << "Ingrese el numero a agregar: ";
        getline(cin, line);
        numero = stoi(line);
        // Creamos los nodos
        lista2->crear(numero);
    }

    // Llamamos la funcion para mostrar nuestras listas
    impresion_listas(lista1, lista2);
    // Luego ordenaremos ambas listas y las impriremos nuesvamente
    lista1->ordenar();
    lista2->ordenar();
    cout << endl;
    cout << "Sus listas han sido ordenadas" << endl;
    cout << endl << endl;
    impresion_listas(lista1, lista2);
    lista3 = lista3->unir(lista1, lista2);
    lista3->ordenar();
    // ANteriormente fusionamos y ordenamos las listas para entregarlas al usuario
    cout << endl;
    cout << "Las listas se han unido y ordenado, lo que resulta en: " << endl;
    cout << "┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼ Lista 3  ┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼" << endl;
    // Llamamos a la funcion que nos imprime cada nodo
    lista3->imprimir();

    return 0;
}
