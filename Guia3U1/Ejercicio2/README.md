# Ejercicio N°2 Guia 3

## Descripcion del programa 
El programa consiste en agregar datos a dos listas a las cuales previamente se les pedirá el tamaño para generar un bucle más limpio y para no dejar datos que podrían querer ser agregados a la lista por culpa de alguna condición dentro del codigo. Luego de que ambas listas están llenas de datos, estas se ordenan y se unen a una tercera lista.

## Problemas en el programa
Se tuvo el problema con los nodos ya que se generaba un error con los headers al incluirlos en el programa principal, por lo que se resolvió agregando la estructura de los nodos a la lista.h que era donde se utilizaban. También se tuvo un pequeño conflicto con generar un archivo adicional a los actuales que era el que nos iniciaba una lista de tipo Lista, se eliminó ese archivo ya que era redundante en la ejecución y se dejó como está actualmente en el código (Lista *algo = new Lista()).

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de tener ambos requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./programa
```

## Funcionamiento del programa
El programa inicialmente pedirá los tamaños para ambas listas, recordar que ambos tamaños pueden ser diferentes, luego de eso se iniciará el completado de ambas listas.
Una vez que se finalice el completado de las listas, se imprimirán ambas listas.
Luego se ordenarán y se mostrarán nuevamente.
Finalmente se mostrará la ultima lista ordenada y unida con las dos listas que se rellenaron anteriormente

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl