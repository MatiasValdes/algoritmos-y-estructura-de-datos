# Ejercicio N°3 Guia 3

## Descripcion del programa 
El programa consiste en agregar datos a una lista por lo que esta vez utilizaremos el 0 para terminar de agregar datos y a su vez dejandolo excluido de ser agregado. Una vez que se terminan de agregar datos de tipo numerico entero a la lista, esta procedera a ordenarse y a rellenar los datos vacios.
* Ejemplo: Si el usuario agregó 1-3-5, el programa le entregará como resultado 1-2-3-4-5

## Problemas en el programa
Se tuvo el problema con los nodos ya que se generaba un error con los headers al incluirlos en el programa principal, por lo que se resolvió agregando la estructura de los nodos a la lista.h que era donde se utilizaban. También se tuvo un pequeño conflicto con generar un archivo adicional a los actuales que era el que nos iniciaba una lista de tipo Lista, se eliminó ese archivo ya que era redundante en la ejecución y se dejó como está actualmente en el código (Lista *algo = new Lista()).

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de tener ambos requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./programa
```

## Funcionamiento del programa
* El programa inicialmente desplegará un bucle en el cual irá pidiendo numeros de tipo numerico enteros hasta que el usuario desee dejar de agregar datos con el numero 0. 
* Luego de eso el programa le mostrará la lista ordenada en el caso de que el usuario haya ingresado los datos aleatoriamente, así también nos servirá a la hora de rellenar ya que tendremos el dato inicial y final.
* Finalmente, el programa rellenará los vacíos y los entregará al usuario en una nueva lista que se mostrará en terminal

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl