#include <iostream>
using namespace std;
#ifndef LISTA_H
#define LISTA_H

//Se define estructura nodo
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

// Se define la clase lista
class Lista{
    // Atributos privados de la lista, como lo son los nodos a utilizar
    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;
    // Atributos publicos de la lista
    public:
        // Constructor por defecto
        Lista();
        // Metodo para crear un nodo
        void crear(int numero);
        // Metodo para ordenar los nodos en la lista
        void ordenar();
        // Metodo para imprimir
        void imprimir();
        // Metodo para rellenar la lista
        Lista* rellenar();
};
#endif
