#include <iostream>
using namespace std;

#include "Lista.h"

// Funcion principal
int main(int argc, char* argv[]){
    Lista *lista = new Lista();
    Lista *nueva = new Lista();
    // Variables de apoyo al codigo
    string line;
    int opcion=0, numero=30;
    // Se prefiere realizar un bucle ya que no sabemos cuantos datos querrá agregar el usuario
    cout << "Ingrese 0 para dejar de agregar datos a la lista" << endl;
    while(numero != 0){
        cout << "Ingrese el numero: ";
        getline(cin, line);
        numero = stoi(line);
        if(numero != 0){
            // Creamos el nodo con el dato ingresado por el usuario
            lista->crear(numero);
            // Además le vamos haciendo seguimiento a la lista
            lista->imprimir();
        }

    }
    // Ordenamos la lista para poder agregar los datos faltantes
    lista->ordenar();
    cout << endl << endl << endl;
    cout << "Su lista ordenada es" << endl;
    lista->imprimir();
    // Entregamos la impresion de los datos finales del usuario
    // Llamamos a la funcion que nos completa la lista
    nueva = lista->rellenar();
    // Luego imprimimos la nueva lista rellenada
    cout << "-------------------------------------------------" << endl;
    cout << "Su lista rellenada quedo de la siguiente forma" << endl;
    nueva->imprimir();

    return 0;
}
