#include <iostream>
using namespace std;
#include "Lista.h"

Lista::Lista(){
    Nodo *primero = NULL;
    Nodo *ultimo = NULL;
}

// Metodo que nos creara el nodo del primer dato
void Lista::crear(int numero){
    // Creamos el nodo
    Nodo *temp = new Nodo;
    // Asignamos el valor al nodo
    temp->numero = numero;
    // Por defecto apuntará a NULL
    temp->sig = NULL;

    // Verificamos si es el primer nodo en la lista
    if(this->primero == NULL){
        this->primero = temp;
        this->ultimo = this->primero;
    } else{
        // Si es que no es el primer nodo en la lista este pasara, el actual apuntara al creado
        this->ultimo->sig = temp;
        // Dejamos como ultimo al nodo que se creo
        this->ultimo = temp;
    }
}

void Lista::ordenar(){
    // Necesitaremos utilizar nodos auxiliares, se utiliza el concepto de ordenamient por burbuja
    Nodo *primero = this->primero;
    Nodo *sucesor = NULL;
    int aux = 0;

    // Se debe recorrer la lista mientras el que sigue al primero no sea nulo
    while (primero->sig != NULL){
        // Nodo siguiente toma el valor del sucesor del primer nodo
        sucesor = primero->sig;
        while(sucesor != NULL){
            // Vamos realizando el ordenamiento por burbuja si es que el numero del primer nodo
            // es mayor que el siguiente
            if(primero->numero > sucesor->numero){
                // Realizamos los pasos para hacer el ordenamiento
                aux = sucesor->numero;
                sucesor->numero = primero->numero;
                primero->numero = aux;
            }
            // Se avanza de nodo para seguir comparando con el siguiente
            sucesor = sucesor->sig;
        }
        // Se avanza nuevamente de nodo para seguir comparando
        primero = primero->sig;
    }
}

// Metodo para imprimir la lista
void Lista::imprimir(){
    // Necesitamos capturar el valor de la raiz
    Nodo *temp = this->primero;
    cout << "------------------------------------------------" << endl;
    while(temp != NULL){
        cout << "[" << temp->numero << "]-->";
        // Debemos capturar el siguiente nodo
        temp = temp->sig;
    }
    cout << endl;
    cout << "------------------------------------------------" << endl;
}
