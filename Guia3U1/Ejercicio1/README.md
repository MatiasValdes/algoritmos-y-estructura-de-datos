# Ejercicio N°1 Guia 3

## Descripcion del programa 
El programa consiste en agregar numeros enteros a una lista enlazada ordenada. Lo que se realizo fue crear un menu para generar una vision mas bonita para el usuario, la cual cuenta con 4 opciones, la más elemental que es la de agregar elementos enteros, también se tiene la de mostrar la lista, ordenar la lista para que sea una lista enlazada ordenada y la opcion de salir del programa

## Problemas en el programa
Se tuvo el problema con los nodos ya que se generaba un error con los headers al incluirlos en el programa principal, por lo que se resolvió agregando la estructura de los nodos a la lista.h que era donde se utilizaban. También se tuvo un pequeño conflicto con generar un archivo adicional a los actuales que era el que nos iniciaba una lista de tipo Lista, se eliminó ese archivo ya que era redundante en la ejecución y se dejó como está actualmente en el código (Lista *algo = new Lista()).

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de tener ambos requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./programa
```

## Funcionamiento del programa
El programa inicialmente desplegará un menú en el que el usuario deberá elegir que hacer. Poseerá 4 opciones las cuales se detallarán
1) Agregar un numero: el usuario podrá ingresar un dato entero a la lista
2) Ver lista: el usuario podrá observar como se encuentra su lista en ese momento
3) Ordenar lista: el usuario obtendrá su lista actual y luego se le ordenará numericamente
4) Salir: el usuario podrá salir del programa, dejandole un mensaje de agradecimiento

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl