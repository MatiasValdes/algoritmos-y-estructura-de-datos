#include <iostream>
using namespace std;

// Clases
#include "Lista.h"

// Funcion del menú
int menu(){
    string opcion;
    // Se imprimen las opciones del usuario
    cout << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "Ingrese una de las siguientes opciones" << endl;
    cout << "[1] Agregar numero" << endl;
    cout << "[2] Ver lista" << endl;
    cout << "[3] Ordenar lista" << endl;
    cout << "[4] Salir " << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "¿Que desea hacer?  ";
    // Capturamos la opcion y la devolvemos en formato numerico
    getline(cin, opcion);
    return stoi(opcion);

}

// Funcion principal
int main(int argc, char* argv[]){
    Lista *datos = new Lista();
    // Variables de apoyo al codigo
    string line;
    int opcion=0, numero=0, indicador=40;
    // Ciclo para mantener el menu
    while(indicador != 0){
        // Capturamos la opcion del menu
        opcion = menu();
        // Agregamos un numero a la lista
        if(opcion == 1){
            cout << "Ingrese numero entero a agregar: ";
            getline(cin, line);
            numero = stoi(line);
            // Creamos un nodo con el numero
            datos->crear(numero);
        }
        // Impresión de la lista como se encuentre en el momento
        else if(opcion == 2){
            cout << endl;
            cout << "Su lista actualmente se encuentra de la siguiente forma" << endl;
            datos->imprimir();
            cout << endl;
        }
        // Se ordena la lista y además se imprime el antes y el despues
        else if(opcion == 3){
            cout << endl;
            cout << "Su lista actualmente es:" << endl;
            datos->imprimir();
            datos->ordenar();
            cout << "Su lista se ha ordenado y ahora es" << endl;
            datos->imprimir();
            cout << endl;

        }
        // Finalizacion del programa
        else if(opcion == 4){
            cout << "Gracias por utilizar el programa" << endl;
            indicador = 0;
        }
        // Opcion numerica no valida
        else{
            cout << "Ingrese una opcion valida" << endl;
        }
    }
    return 0;
}
