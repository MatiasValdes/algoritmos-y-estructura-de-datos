using namespace std;

#ifndef ELEMENTO_H
#define ELEMENTO_H

// Se define la clase elemento
class Elemento {
    // Como dato privado el numero y el cuadrado del mismo
    private:
        int numero = 0;
        int cuadrado = 0;
    // Como publico los metodos
    public:
        // Constructor por defecto
        Elemento();
        // Metodos set and get además del cuadrado del numero
        void set_numero(int numero);
        void set_cuadrado();
        int get_numero();
        int get_cuadrado();

};
#endif
