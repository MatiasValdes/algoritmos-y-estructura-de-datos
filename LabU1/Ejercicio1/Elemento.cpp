#include <iostream>
using namespace std;
#include "Elemento.h"

// Constructor por defecto
Elemento::Elemento(){
    int numero = 0;
    int cuadrado = 0;
}

// Metodo para setear el numero
void Elemento::set_numero(int numero){
    this->numero = numero;
}

// Metodo para obtener el cuadrado del mismo numero
void Elemento::set_cuadrado(){
    this->cuadrado = (this->numero * this->numero);
}

// Metodo que devuelve el cuadrado del numero
int Elemento::get_cuadrado(){
    return this->cuadrado;
}
// Metodo que nos devuelve el numero
int Elemento::get_numero(){
    return this->numero;
}
