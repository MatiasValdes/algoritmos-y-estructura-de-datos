#include <iostream>
using namespace std;
#include "Elemento.h"


// Funcion que suma los cuadrados de los numeros
int sumar_cuadrados(int *arreglo, int largo){
    int suma=0;
    // Ciclo para sumar los cuadrados de los numeros
    for(int i=0; i<largo;i++){
        // La operacion
        suma = arreglo[i] + suma;
    }
    // retorna la suma de los cuadrados
    return suma;
}


// Funcion principal
int main(int argc, char* argv[]){
    // Se inicializan las variables que se utilizarán
    int largo, suma=0, digito;
    // Se pide un numero para el total de datos
    cout << "Ingrese la cantidad de numeros que ingresará:" << endl;
    cin >> largo;
    // Un arreglo del largo ingresado
    int arreglo[largo];

    // Ciclo que obtendrá los numeros
    for(int i=0; i<largo; i++){
        cout << "Ingrese un numero: ";
        cin >> digito;
        // Se crea un elemento que tiene como valor el numero
        Elemento numero = Elemento();
        numero.set_numero(digito);
        // Se realiza el cuadrado del numero
        numero.set_cuadrado();
        // Se ingresa al arreglo el cuadrado del elemento
        arreglo[i] = numero.get_cuadrado();
    }
    // Se busca la suma
    suma = sumar_cuadrados(arreglo, largo);
    // Impresión del valor
    cout << "La suma de los cuadrados de los numeros ingresados es " << suma << endl;
    return 0;
}
