#include <iostream>
#include <cctype>
using namespace std;
#include "Frase.h"

// Constructor de la clase
Frase::Frase(){
    string frase = "\0";
    int mayusculas = 0;
    int minusculas = 0;
}

// Metodo para setear la clase
void Frase::set_frase(string frase){
    this->frase = frase;
}

// Metodo para contar las mayusculas
void Frase::contar_mayusculas(){
    for(int i=0; i<frase.size();i++){
        // Ocupamos la funcion isupper
        if(isupper(frase[i])){
            // almacenamos la suma
            this->mayusculas = this->mayusculas + 1;
        }
    }
}

// Metodo para contar las minusculas
void Frase::contar_minusculas(){
    for(int i=0; i<frase.size();i++){
        // En este caso utilizamos la funcion islower
        if(islower(frase[i])){
            this->minusculas = this->minusculas + 1;
        }
    }
}

// Metodos get de frase, el total de mayusculas como de minusculas
string Frase::get_frase(){
    return this->frase;
}

int Frase::get_mayusculas(){
    return this->mayusculas;
}

int Frase::get_minusculas(){
    return this->minusculas;
}
