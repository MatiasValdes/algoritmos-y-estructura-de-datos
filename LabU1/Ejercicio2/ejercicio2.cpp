#include <iostream>
using namespace std;
#include "Frase.h"

// Funcion que nos permitira crear frases y verificar
void verificar_frases(Frase *frases, int largo){
    // Recorremos el arreglo
    for(int i=0;i<largo;i++){
        // Ejecutamos los metodos que nos permiten contar mayusculas y minusculas
        frases[i].contar_mayusculas();
        frases[i].contar_minusculas();
        // Impresión final de lo solicitado
        cout << "La frase: " << frases[i].get_frase() << " posee" << endl;
        cout << "Mayusculas: " << frases[i].get_mayusculas() << endl;
        cout << "Minusculas: " << frases[i].get_minusculas() << endl << endl << endl;
    }

}
int main(int argc, char* argv[]){
    int largo;
    string line, large;
    // Pedimos el tamaño de las frases que se ingresaran
    cout << "Ingrese el numero de frases que ingresará: ";
    getline(cin, large);
    largo = stoi(large);
    // Creamos un arreglo que nos almacenará las frases
    Frase frases[largo];
    // Realizamos el ciclo y almacenamos las frases que ingreso el usuario
    for(int i=0; i<largo;i++){
        // Obtenemos la frase
        cout << "Ingrese la frase " << i + 1 << endl;
        getline(cin, line);
        // Agregamos la frase al arreglo seteando la frase 
        frases[i].set_frase(line);
        cout << endl;
      }
    // Llamamos a la funcion
    verificar_frases(frases, largo);
    return 0;
}
