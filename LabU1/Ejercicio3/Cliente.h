using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

// Definicion de la clase cliente
class Cliente {
    // Atributos privados de la clase
    private:
        string nombre = "\0";
        string telefono = "\0";
        int saldo = 0;
        bool moroso = 0;
    // Atributos publicos de la clase
    public:
        // constructor
        Cliente();
        // Metodos set de la clase
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);
        // Metodos get de la clase
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();

};
#endif
