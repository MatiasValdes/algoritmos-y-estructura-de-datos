#include <iostream>
using namespace std;
#include "Cliente.h"

// Definicion del constructor
Cliente::Cliente(){
    string nombre = "\0";
    string telefono = "\0";
    int saldo = 0;
    bool moroso = 0;
}

// Metodos set del nombre, telefono, saldo y moroso
void Cliente::set_nombre(string nombre){
    this->nombre = nombre;
}

void Cliente::set_telefono(string telefono){
    this->telefono = telefono;
}

void Cliente::set_saldo(int saldo){
    this->saldo = saldo;
}

void Cliente::set_moroso(bool moroso){
    this->moroso = moroso;
}

// Metodos get del nombre, telefono, saldo y moroso
string Cliente::get_nombre(){
    return this->nombre;
}

string Cliente::get_telefono(){
    return this->telefono;
}

int Cliente::get_saldo(){
    return this->saldo;
}

bool Cliente::get_moroso(){
    return this->moroso;
}
