#include <iostream>
using namespace std;
#include "Cliente.h"

// Funcion que nos agregara los clientes
void agregar_clientes(Cliente *clientes, int largo){
    string nombre, telefono, moroso;
    int saldo;
    cout << "Ingresar informacion de los clientes" << endl;
    // Ingresamos los datos de cada cliente
    for(int i=0;i<largo; i++){
        // Pedimos los datos que se requieren de cada cliente
        cout << "Ingrese el nombre del cliente: ";
        getline(cin, nombre);
        cout << "Ingrese el numero del cliente: ";
        getline(cin, telefono);
        cout << "Ingrese el saldo del cliente: ";
        cin >> saldo;
        cout << "¿El cliente es moroso? \nIngrese 1 para si \nIngrese 2 para no" << endl;
        getline(cin, moroso);
        // Realizamos un ciclo para que siempre se ingresen esos datos que se
        // definieron y no generar un conflicto en el programa que haga que se
        // caiga
        while(moroso != "1" && moroso != "2"){
            cout << "Ingrese una opcion de las dadas anteriormente "<< endl;
            getline(cin, moroso);
        }
        // agregamos los datos de los clientes al arreglo
        clientes[i].set_nombre(nombre);
        clientes[i].set_telefono(telefono);
        clientes[i].set_saldo(saldo);
        // condicion para setear el valor del moroso como booleano
        if(moroso == "1"){
            clientes[i].set_moroso(1);
        }
        else{
            clientes[i].set_moroso(0);
        }
        cout << endl;
    }
}

// Funcion que nos permite imprimir los clientes
void imprimir_clientes(Cliente *clientes, int largo){
    cout << "Los clientes ingresados son: " << endl;
    // Ciclo para recorrer el arreglo de los clientes
    for(int i=0; i<largo;i++){
        // Impresion
        cout << "Cliente N°" << i+1 << endl;
        cout << "Nombre: " << clientes[i].get_nombre() << endl;
        cout << "Telefono: " << clientes[i].get_telefono() << endl;
        cout << "Saldo: " << clientes[i].get_saldo() << endl;
        if(clientes[i].get_moroso()){
              cout << "El cliente es moroso" << endl << endl;
        }
        else{
            cout << "El cliente no es moroso" << endl << endl;
        }
    }
}

// Funcion principal
int main(int argc, char* argv[]){
    // variables
    int largo;
    string numero;
    // Pedimos la cantidad de clientes a agregar
    cout << "Ingrese la cantidad de clientes a guardar" << endl;
    getline(cin, numero);
    largo = stoi(numero);
    // Creamos un arreglo de tipo Cliente
    Cliente clientes[largo];
    // Llamamos a la funcion que nos agregara a los clientes
    agregar_clientes(clientes, largo);
    // Funcion que nos imprimira los clientes
    imprimir_clientes(clientes, largo);

    return 0;
}
