# Guia Numero 9 unidad III

## Descripcion del programa 
El programa genera un arreglo según los datos ingresados por el usuario además del tamaño, además el usuario podrá elegir entre las diferentes pruebas para resolver colisiones ya sea prueba lineal (l-L), prueba cuadratica (c-C), prueba de doble direccion(d-D) y metodo de encadenamiento(e-E)

## Problemas en el programa
El programa no presentó problemas en la creación y ejecución

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make


En caso de tener los requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./hash  {c,l,e,d}
```

## Funcionamiento del programa
El programa pedirá 1 parametros, el cual corresponde al tipo de metodo para solucionar colisiones

#### A partir de ahí
El usuario deberá ingresar el tamaño maximo de su arreglo para así ir agregando datos o buscando datos en el arreglo, según el metodo de colision que haya elegido se arreglarán las colisiones que se podrían generar en las posiciones según el modulo hash que fue implementado además también se implementará el mismo metodo para buscar algún dato que el usuario desee conocer su posicion

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl