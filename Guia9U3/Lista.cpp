#include <iostream>
using namespace std;
#include "Lista.h"

Lista::Lista(){
    Nodo *primero = NULL;
    Nodo *ultimo = NULL;
}

// Metodo que nos creara el nodo del primer dato
void Lista::crear(int numero){
    // Creamos el nodo
    Nodo *temp = new Nodo;
    // Asignamos el valor al nodo
    temp->numero = numero;
    // Por defecto apuntará a NULL
    temp->sig = NULL;

    // Verificamos si es el primer nodo en la lista
    if(this->primero == NULL){
        this->primero = temp;
        this->ultimo = this->primero;
    } else{
        // Si es que no es el primer nodo en la lista este pasara, el actual apuntara al creado
        this->ultimo->sig = temp;
        // Dejamos como ultimo al nodo que se creo
        this->ultimo = temp;
    }
}

// Metodo para imprimir la lista
void Lista::imprimir(){
    // Necesitamos capturar el valor de la raiz
    Nodo *temp = this->primero;
    cout << "------------------------------------------------" << endl;
    while(temp != NULL){
        cout << "[" << temp->numero << "]-->";
        // Debemos capturar el siguiente nodo
        temp = temp->sig;
    }
    cout << endl;
    cout << "------------------------------------------------" << endl;
}