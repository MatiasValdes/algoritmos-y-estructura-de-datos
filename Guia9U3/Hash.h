#ifndef HASH_H
#define HASH_H
#include <iostream>
#include <stdlib.h>
#include "Nodo.h"
#include "Lista.h"
using namespace std;

// Creacion de la clase Hash
class Hash{
    // No hay atributos privados
    private:

    // Atributos publicos
    public:
        // Constructor por defecto
        Hash();
        // Modulos hash por división como además del aux 
        int modulo_hash(int clave, int primo);
        int modulo_hash_aux(int clave, int primo);
        // Metodo para la prueba cuadratica
        void prueba_cuadratica(int arreglo[], int tamanio, int numero, int primo);
        // Metodo para la prueba lineal
        void prueba_lineal(int arreglo[], int tamanio, int numero, int primo);
        // Metodo para la prueba de doble direccion
        void doble_direccion(int arreglo[], int tamanio, int numero, int primo);
        // Metodo que hace un copiado del arreglo
        void transformar_arreglo(Nodo *nodo2[],int arreglo[], int tamanio);
        // Metodo que imprime la lista que acompaña al numero colisionado
        void imprimir_lista(Nodo *nodo2[],int aux);
        // Metodo para la prueba de encadenamiento
        void encadenamiento(int arreglo[], int tamanio, int numero, int primo, Lista *lista);
};  
#endif