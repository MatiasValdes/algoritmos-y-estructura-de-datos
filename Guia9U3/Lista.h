#include <iostream>
using namespace std;
#ifndef LISTA_H
#define LISTA_H
#include "Nodo.h"

// Se define la clase lista
class Lista{
    // Atributos privados de la lista, como lo son los nodos a utilizar
    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;
    // Atributos publicos de la lista
    public:
        // Constructor por defecto
        Lista();
        // Metodo para crear un nodo
        void crear(int numero);
        void imprimir();
};
#endif