#include <iostream>
#include <stdlib.h>
using namespace std;
#include "Hash.h"
#include "Lista.h"

// Funcion del menú
int menu(){
    string line;
    int opcion=0;
	// Se entregan las opciones al usuario
	cout << "----------------------------------------" << endl;
	cout << "              [ MENU ]" << endl;
	cout << "----------------------------------------" << endl;
	cout << " [1] Agregar numeros al arreglo" << endl;
	cout << " [2] Buscar numero en el arreglo" << endl;
	cout << " [3] Salir del programa" << endl;
	cout << "----------------------------------------" << endl;
	cout << " Opcion: ";
	// Se capta la opción
	getline(cin,line);
	opcion = stoi(line);
	// Validamos de que la opcion ingresada sea correcta
	while((opcion > 3)  || (opcion < 1)){
		// Hasta que ingrese una opcion correcta no sale de este bucle
		cout << "\nOpcion no valida, por favor ingrese una valida " << endl;
		cout << "Opcion: ";
		getline(cin,line);
        opcion = stoi(line);
	}
	// Retornamos la opcion elegida
	return opcion;
}


// Funcion que nos comprueba el metodo que fue ingresado
bool comprobar_metodo(string metodo_colision){
	// Se crea un arreglo con todos los caracteres posibles
	string caracteres[8] = {"c", "C", "l", "L", "e", "E", "D", "d"};
	int contador = 0;
	bool correcto = false;
	// Se recorre el arreglo
	for(int i=0; i<8;i++){
		// Y nos fijamos si el metodo se encuentra en alguna posicion
		if(caracteres[i] == metodo_colision){
			// El contador solo podría aumentar una sola vez
			contador++;
		}
	}
	// Por ello si pudo ser aumentado devolvemos un true para hacer la condicion
	if(contador == 1){
		return true;
	}
	// De lo contrario el metodo de colision entregado es erroneo
	else{
		return false;
	}
}


// Funcion que nos imprimira el arreglo actual cuando sea necesario
void imprimir_arreglo(int arreglo[], int tamanio){
	cout << endl;
    cout << "----------------------------------------" << endl;
	cout << "--- ARREGLO ---" << endl;
	// Se imprime el arreglo con un diseño facil elegido para la vision del usuario
	for(int i=0; i<tamanio; i++){
		if(arreglo[i] == 0){
			cout << "[*]-";
		}
		else{
			cout << "["<< arreglo[i] << "]-";
		}
	}
	cout << endl;
	cout << "----------------------------------------" << endl;
}


//Funcion para llenar un arreglo vacio
void rellenar_arreglo(int arreglo[], int tamanio){
	for(int i=0; i<tamanio; i++){
		// Agregamos el arreglo con vacíos para hacer las condiciones cuando sean necesarias
		arreglo[i] = '\0';
	}
}


// Funcion que nos busca el numero primo según el tamaño del usuario
int buscar_primo(int tamanio){
    int primo=0, aux=0;
    for (int i=2;i<=tamanio;i++){
        aux=0;
        for(int j=1;j<=tamanio;j++){
			// si i módulo de j es 0, incrementamos aux en 1.
            if(i%j==0) 
                aux++;
            }
		//si solo tiene dos números divisores entonces es primo y se guarda momentaneamente
        if(aux == 2){ 
            primo = i;
        }
    }
	// Devuelve el primo más cercano al tamaño ingresado por el usuario
    return primo;
}
    

// Funcion que nos comprueba si el arreglo se encuentra lleno
bool comprobar_lleno(int arreglo[], int tamanio){
	bool lleno=false;
	int contador = 0;
	// Recorremos el arreglo buscando a los datos distintos a vacio
	for(int i = 0; i<tamanio;i++){
		if(arreglo[i] != '\0'){
			// Agregamos cuando son distintos a vacio
			contador++;
		}
	}
	// Si es igual al tamaño se vuelve true
	if(contador == tamanio){
		lleno = true;
	}
	// De lo contrario false
	else{
		lleno = false;
	}
	return lleno;
}


// Funcion que comprueba que metodo de insercion se ocupará y lo deriva
void prueba_insercion(int arreglo[], int tamanio, int numero, int primo, string metodo_colision, Lista *lista){
    Hash *hash = new Hash();
    // Prueba lineal
    if((metodo_colision.compare("L")==0) || (metodo_colision.compare("l")==0)){
		// Se ocupa prueba lineal
        hash->prueba_lineal(arreglo, tamanio, numero, primo);
	}
	// Prueba cuadratica
	if((metodo_colision.compare("C")==0) || (metodo_colision.compare("c")==0)){
		// Se ocupa prueba cuadratica
        hash->prueba_cuadratica(arreglo,tamanio, numero, primo);
	}
	// Prueba doble direccion
	if((metodo_colision.compare("D")==0) || (metodo_colision.compare("d")==0)){
		// Se ocupa prueba de doble direccion
        hash->doble_direccion(arreglo, tamanio, numero, primo);
	}
	// Prueba encadenamiento
	if((metodo_colision.compare("E")==0) || (metodo_colision.compare("e")==0)){
		// Se ocupa encadenamiento
        hash->encadenamiento(arreglo, tamanio, numero, primo, lista);
	}
}


// Funcion principal
int main(int argc, char* argv[]){
	Lista *lista = new Lista();
	system("clear");
    // Obtenemos el metodo de colision que ingreso el usuario como parametro
    string metodo_colision;
	bool correcto;
    if(argc == 2){
        // Guardamos el metodo
		metodo_colision = argv[1] ;
	}
    // Se envía un mensaje de que falto un parametro para que se ejecute nuevamente 
	else{
		cout << "-- Se necesita 1 parametro --" << endl;
		return 1;
	}
	// Nos verifica que el metodo entregado por el usuario realmente sea uno de los posibles
	correcto = comprobar_metodo(metodo_colision);
	if(correcto == false){
		cout << "Ingrese un metodo valido" << endl;
		return 1;
	}
    // Variables de apoyo al codigo
    string line;
    int tamanio=0, numero=0, opcion=4, primo=0, posicion=0;
	bool lleno=false;

    // Se pide el tamaño del arreglo que desea el usuario
    cout << "Ingrese el tamaño del arreglo" << endl;
    cout << "Tamaño: ";
    getline(cin, line);
    tamanio = stoi(line);

    // Funcion que nos devolverá el primo para generar el modulo de hash
    primo = buscar_primo(tamanio);
    // Creamos un arreglo con el tamaño del usuario
    int arreglo[tamanio];
    Hash *hash = new Hash();
    // Rellenamos el arreglo con datos que nos permiten comparar
    rellenar_arreglo(arreglo, tamanio);

    // Iniciamos el bucle
    while(opcion != 3){
        // Obtenemos la opcion pedida por el usuario
        opcion = menu();
        if(opcion == 1){
			// Antes de agregar nos fijamos si el arreglo esta lleno o no
			lleno = comprobar_lleno(arreglo, tamanio);
			// Si esta arreglo pasa
			if(lleno == true){
				cout << "El arreglo se encuentra lleno" << endl;
			}
			// Sino aun se puede agregar
			else{
				// Capturamos numero
				cout << "Numero: ";
				getline(cin, line);
				numero = stoi(line);
				// Sacamos la posicion del numero
				posicion = hash->modulo_hash(numero, primo);
				//Si no existe nada en esa posicion se asigna e imprime 
				if(arreglo[posicion] == '\0'){
					arreglo[posicion] = numero;
					imprimir_arreglo(arreglo, tamanio);
				}
				//De lo contrario existe colision y se debe ejecutar algun metodo
				else{
					cout << "Colision en posicion: " << posicion + 1 << endl;
					// Se llama a la prueba de insercion que contiene a los metodos y se imprime como queda
					prueba_insercion(arreglo, tamanio, numero, primo, metodo_colision, lista);
					imprimir_arreglo(arreglo, tamanio);
				}
			}
        }
		// Para buscar un elemento
        else if(opcion == 2){
			// Mostramos como se encuentra el arreglo actualmente 
			cout << "El arreglo se encuentra de la siguiente forma" << endl;
			imprimir_arreglo(arreglo, tamanio);
			// Comprobamos si esta lleno
			lleno = comprobar_lleno(arreglo, tamanio);
            // Si arreglo no esta vacio entonces se puede buscar
			if(lleno == false){
				// Capturamos el numero
				cout << "Ingrese numero: ";
			    getline(cin,line);
			    numero = stoi(line);
				// Obtencion de posicion con Hash
				posicion = hash->modulo_hash(numero, primo);
				// Si se encuentra el numero en esa posicion se imprime 
				if(arreglo[posicion] == numero){
					cout << "Clave se encuentra en posicion: " << posicion + 1 << endl;
					imprimir_arreglo(arreglo, tamanio);
				}
				// Sino debemos ejecutar una prueba de insercion para agregar el numero
				else{
					prueba_insercion(arreglo, tamanio, numero, primo, metodo_colision, lista);
					imprimir_arreglo(arreglo, tamanio);
				}
			}
			// Sino el arreglo está vacio
			else{
				cout << "El arreglo se encuentra vacio" << endl << endl;
			}
        }
		// Salida del programa
        else{
            cout << "Finalización del programa con éxito" << endl;
            return 0;
        }
    }
    
}
