# Guia Numero 8 unidad III

## Descripcion del programa 
El programa generara un arreglo aleatorio con el tamaño ingresado por el usuario, luego ese arreglo será ordenado con el metodo "Selection" y el metodo "Quicksort" tomando el tiempo que se demoran en cada uno de estos metodos.

## Problemas en el programa
El programa no presentó problemas en la creación y ejecución

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make


En caso de tener los requerimientos pasar a la compilación y ejecución del programa

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar

## Compilacion y ejecución del programa
* Dentro de la terminal debe dirigirse al lugar donde se encuentra la carpeta con los ejercicios 
* Una vez que este dentro de la carpeta de ejercicio debe ejecutar 
```
make
```
* Una vez que se compile el ejercicio, deberá proseguir con la ejecución del programa en el cual deberá hacer lo siguiente dentro de la misma carpeta
```
./programa
```

## Funcionamiento del programa
El programa pedirá 2 parametros, el primero será el tamaño del arreglo y como segundo parametro posee 2 opciones

### Opcion 1: el usuario ingrese la letra "s", cuando el usuario ingrese esta letra el programa mostrará el arreglo inicial y los ordenados junto con los tiempos que se demoró cada uno de estos metodos
### Opcion 2: el usuario ingrese la letra "n", en este caso el programa solo mostrará el arreglo inicial aleatorio y los tiempos que se demoró el programa en ordenarlo con ambos metodos

El primer párametro debe ser un tipo númerico entero

## Autor
* Matías Valdés
* Correo: matvaldes19@alumnos.utalca.cl