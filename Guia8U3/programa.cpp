#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include "Ordenar.h"

// Definimos el limite superior e inferior de los datos
#define LIMITE_SUPERIOR 1000000
#define LIMITE_INFERIOR 1

// Funcion que nos imprimira nuestro arreglo principal generado aleatoriamente
void imprimir_arreglo(int arreglo[], int numero){
    for(int i=0;i<numero;i++){
        cout << "[" << i << "] = " << arreglo[i] << " --- ";
    }
    cout << endl;
}


// Funcion principal
int main(int argc, char* argv[]){
    system("clear");
    // Variables a utilizar dentro del codigo
    int numero = 0;
    int i=0;
    int elemento=0;
    bool mostrar;
    string letra;
    // Ocupamos la libreria de chrono para generar los 2 tiempos para el ordenamiento
    chrono::duration<double> tiempo1, tiempo2;
    // Nuestra clase ordenar
    Ordenar *ordenar;
    // Si se entregan los 3 parametros
    if(argc == 3){
        // Se captura el numero
        numero = atoi(argv[1]);
        // Capturamos la letra acompañada
        letra = argv[2];
        // Validamos de que el numero que este en el rango
		if (numero > LIMITE_SUPERIOR ){
			cout << "Numero invalido para la ejecucion del programa" << endl;
			cout << "-- Debe ser menor o igual a 1.000.000 --" << endl;
			return 1;
		}
    }else{
        cout << "-- Se necesitan 2 parametros -- " << endl;
		return 1;
    }

    // Declaracion de los arreglos
    int lista[numero];
    int seleccion[numero];
    int quick[numero];
    srand(time(NULL));

    // Generacion del arreglo aleatorio
    for(i=0; i<numero; i++){
        elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
        lista[i] = elemento; 
    }

    // Se imprime el arreglo que se genero
    cout << "Se generó el siguiente arreglo aleatorio" << endl;
    imprimir_arreglo(lista, numero);
    cout << endl << endl << endl;
    
    // Generamos dos arreglos nuevos con el arreglo generado aleatorio esto para prevenir
    // el caso de que se ordene y luego el proximo metodo ya lo tenga ordenado
    for(int i=0;i<numero;i++){
        seleccion[i] = lista[i];
        quick[i] = lista[i];
    }
    // Vemos si se desea mostrar la impresion del arreglo ya arreglado
    if(letra == "s"){
        // Si es "s" se vuelve true
        mostrar = true;
    }
    // Sino se vuelve falso
    else{
        mostrar = false;
    }

    // Metodo seleccion
    // Ocupamos la libreria de chrono para tomar el tiempo
    auto tiempo_inicial = chrono::high_resolution_clock::now();
    // Ordenamos el arreglo con el metodo seleccion 
    ordenar->seleccion(seleccion, numero, mostrar);
    // Tomamos el tiempo luego de ordenar
    auto tiempo_final = chrono::high_resolution_clock::now();
    // Guardamos el tiempo
    tiempo1 = tiempo_final-tiempo_inicial;

    // Metodo quicksort
    // Ocupamos nuevamente la librería chrono
    tiempo_inicial = chrono::high_resolution_clock::now();
    ordenar->quicksort(seleccion, numero, mostrar);
    tiempo_final = chrono::high_resolution_clock::now();
    // Guardamos el tiempo
    tiempo2 = tiempo_final-tiempo_inicial;
    // Se imprimen los resultados de tiempo para cada metodo
    
    cout << endl;
    cout << "Metodo\t|\tTiempo" << endl;
    cout << "Seleccion | " << tiempo1.count() << " milisegundos" << endl;
    cout << "Quicksort | " << tiempo2.count() << " milisegundos" << endl;

    return 0;
}
