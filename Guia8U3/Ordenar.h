#ifndef ORDENAR_H
#define ORDENAR_H
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

// Creacion de la clase ordenar
class Ordenar{
    // No hay atributos privados
    private:

    public:
        // Constructor por defecto
        Ordenar();
        // Metodo para el ordenamiento por seleccion
        void seleccion(int arreglo[], int numero, bool mostrar);
        // Metodo de reduce del quicksort
        void reduce_quicksort(int arreglo[], int inicio, int fin, int &pos);
        // Metodo para el ordenamiento por quicksort
        void quicksort(int arreglo[], int numero, bool mostrar);
        // Metodo para imprimir arreglos
        void imprimir(int arreglo[], int numero, string metodo);

};
#endif