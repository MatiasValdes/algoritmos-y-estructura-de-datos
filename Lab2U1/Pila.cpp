#include <iostream>
#include <stdlib.h>
using namespace std;
#include "Pila.h"

// Constructor con el largo
Pila::Pila(int largo){
    // Atributos de la clase
    this->largo = largo;
    this->tope = -1;
    this->arreglo = NULL;
    this->max = 0;
    bool band;
    this->band = band;
}

// Metodo en el que se crea la pila
void Pila::crear_pila(){
    // Se define el maximo y se crea el arreglo con el tamaño dado por el usuario
    this->max = largo - 1;
    this->arreglo = new int[largo];
}

// Metodo para observar si la pila se encuentra llena
void Pila::Pila_llena(){
    // Si el tope con el max son iguales es porque la pila se encuentra llena
    if(this->tope == this->max){
        // Definimos el booleano como verdadero
        this->band = true;
    }
    // Sino es así es porque aun no esta llena y el booleano será falso
    else{
        this->band = false;
    }
}

// Metodo para ver si la pila se encuentra vacía
void Pila::Pila_vacia(){
    // Si el tope tiene un valor negativo [-1] es porque está vacía
    if(this->tope == -1){
        this->band = true;
    }
    else{
        this->band = false;
    }
}

// Metodo para agregar un dato a la pila
void Pila::push(int dato){
    // Primero debemos asegurarnos de que la pila no esté llena lo que nos hará
    // que cambie el valor del booleano dependiendo de la pila
    Pila_llena();
    // Si el booleano es falso es porque no está llena
    if(this->band == false){
        // Aumentamos el tope
        this->tope = this->tope + 1;
        // Agregamos el dato
        this->arreglo[this->tope] = dato;
        // Se imprime en pantalla que fue agregado con exito
        cout << "Dato agregado con éxito" << endl;
        system("sleep 2");
    }
    // Si el booleano es verdadero es porque esta llena
    else{
        cout << "Desbordamiento, la pila está llena" << endl;
    }
}

// Metodo para eliminar datos de la pila
void Pila::pop(){
    // Primero nos aseguramos que la pila no esté vacia osino no se pueden
    // eliminar datos
    Pila_vacia();
    // Si el bool es falso podemos eliminar datos
    if(this->band == false){
        // Imprimimos el dato eliminado
        cout << "Elemento eliminado [" << this->arreglo[tope] << "]" << endl;
        // Reducimos el tope
        this->tope = this->tope - 1;
        system("sleep 2");
        cout << endl;
    }
    // Sino, la pila está vacia
    else{
        cout << "La pila se encuentra vacía" << endl;
        system("sleep 2");
        cout << endl;
    }
}

// Metodo para imprimir la pila
void Pila::imprimir(){
    // Primero nos aseguramos de que la pila no esté vacia
    Pila_vacia();
    // Si el bool es falso podemos imprimir datos
    if(this->band == false){
        cout << endl;
        // Imprimimos los datos
        for(int i=this->tope;i>=0;i--){
            cout << "|" << this->arreglo[i] << "|" << endl;
        }
        cout << endl;
        system("sleep 2");
    }
    // Sino la pila se encuentra vacía
    else{
        cout << "La pila se encuentra vacía" << endl;
        system("sleep 2");
    }
}
