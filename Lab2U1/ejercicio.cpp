#include <iostream>
using namespace std;
#include "Pila.h"


// Funcion que nos devolverá una opcion del usuario
int menu(){
    string opcion;
    // Se imprimen las opciones del usuario
    cout << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "Ingrese una de las siguientes opciones" << endl;
    cout << "[1] Agregar dato/push" << endl;
    cout << "[2] Remover dato/pop" << endl;
    cout << "[3] Ver pila" << endl;
    cout << "[4] Salir " << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "¿Que desea hacer?  ";
    // Capturamos la opcion y la devolvemos en formato numerico
    getline(cin, opcion);
    return stoi(opcion);

}


// Funcion principal
int main(int argc, char* argv[]){
    // Variables que utilizaremos dentro del codigo
    string largo, numero;
    int opcion = 1;
    // Primeramente pedimos el largo de la pila
    cout << "¿Cuantos elementos ingresará a la pila?" << endl;
    getline(cin, largo);
    // Creamos nuestra pila
    Pila pila = Pila(stoi(largo));
    pila.crear_pila();

    // Ciclo para mantener el ciclo
    while(opcion != 0){
        // Capturamos la opcion del menú
        opcion = menu();
        // Opcion 1 para agregar un elemento
        if(opcion == 1){
            // Pedimos el elemento a agregar
            cout << endl <<  "Ingrese un numero entero" << endl;
            getline(cin, numero);
            // Agregamos el elemento a la pila
            pila.push(stoi(numero));

        }
        // Opcion 2
        else if(opcion == 2){
            // Llamamos al metodo de la clase para remover datos
            pila.pop();
        }
        else if(opcion == 3){
            // Metodo de la funcion para imprimir la pila
            pila.imprimir();
        }
        // Finalizacion del programa
        else if(opcion == 4){
            cout << "Fin del programa" << endl;
            opcion = 0;
        }
        // Si se entrega una opcion no valida numerica
        else{
            cout << "Ingrese una opcion valida" << endl;
        }
    }
    return 0;
}
