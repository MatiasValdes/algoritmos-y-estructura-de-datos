using namespace std;

#ifndef PILA_H
#define PILA_H

// Se define la clase pila
class Pila{
    // Atributos privados
    private:
        // Se definen los atributos privados de la clase
        int largo=0;
        int tope = -1;
        int *arreglo = NULL;
        int max = 0;
        bool band;
    // Atributos publicos que corresponden a los metodos
    public:
        // Constructor
        Pila(int largo);
        // Metodo para crear pila y metodos para observar si la pila esta vacia
        // o está llena
        void crear_pila();
        void Pila_vacia();
        void Pila_llena();
        // Metodo para agregar, remover e imprimir la pila
        void push(int dato);
        void pop();
        void imprimir();
};
#endif
