# Descripción del programa correspondiente al laboratorio 2 unidad 1

Es un programa que permite generar un pila de datos enteros y realizar acciones de push(agregar datos) y pop(eliminar datos) asegurando de no agregar más datos de los que se pueden agregar o remover datos inexistentes. 
Se utiliza lenguaje de programacion C++.

## Compilar el programa
Se necesita tener sistema operativo linux y el compilador make, luego de tener ambos requisitos debemos situarnos donde se haya descargado el laboratorio y ejecutar el comando "make", lo cual nos compilará el ejercicio y nos generará un ejecutable. Este se inicia utilizando ./ejercicio en la misma posición

## Uso del programa
Primeramente se pedirá el maximo de la pila y se desplegará el menú con las opciones. Allí el usuario deberá elegir la opción que le acomode

#### Opcion 1
El usuario podrá ingresar un dato entero a la pila aunque se le avisará si la pila se encuentra llena 

#### Opcion 2
El usuario podrá eliminar el ultimo dato ingresado a la pila aunque se le avisará si la pila se encuentra vacía

#### Opcion 3
El usuario podrá ver la pila pero se le avisará si se encuentra vacía

#### Opcion 4
Salir del programa

### Muchas gracias por utilizar este programa